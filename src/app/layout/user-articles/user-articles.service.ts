import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/internal/Subject';

@Injectable({
  providedIn: 'root'
})
export class UserArticlesService {
  subject1 = new Subject<any>();
  name1: any;
  constructor() {
   }
   sendMessage(message: string) {
    this.subject1.next({ text: message });
    this.name1 = message
    console.log(message,"Deepika")
}

   getMessage(): Observable<any> {
    return this.subject1.asObservable();
}

}

