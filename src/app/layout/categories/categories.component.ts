import { Location } from '@angular/common';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    ValidatorFn,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';
@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.scss'],
    providers: [NgbModalConfig, NgbModal],
})
export class CategoriesComponent implements OnInit {
    // @ViewChild(MatTabGroup) tabGroup: MatTabGroup;
    addModel: Boolean = false;
    isAdminListLoadin = false;
    LanguageList = [];
    LanguageID: any;
    skip = 0;
    limit = 10;
    languages_number: any = 0;
    subcategoryList = [];

    language_id_data = '';
    title = 'appBootstrap';
    model;
    closeResult: string;
    paramsData: any = {};
    adminData: any;
    languageData: any = {};
    categoryData: any = {};
    categoryDataNew: any;
    categoryTitle: any = {};
    Category: any = {};
    AddForm: FormGroup;
    EditForm: FormGroup;
    DeleteForm: FormGroup;
    selectedTabIndex = 0;
    static_data_count = 0;
    static_data = [];
    language_id = 1;
    category_new_id: any = {};
    category_ID = '';
    categoryDropdown: any = {};
    delete_category: any = {};
    delete_category_data: any = {};

    constructor(
        private cookieService: CookieService,
        private appService: AppService,
        private location: Location,
        // private route: ActivatedRoute,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private http: HttpClient
    ) {
        this.adminData = JSON.parse(this.cookieService.get('adminData'));
    }

    ngOnInit() {
        this.paramsData.admin_id = this.adminData.id;
        this.paramsData.session_id = this.adminData.session_id;
        this.selectedIndexChange(0);
       
    }
    List_tll_Languages(data) {
        try {
            this.appService.postMethod('List_All_Languages', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        console.log(resp);
                        this.languageData = resp.data;
                    } else {
                        swal.fire(resp.msg, 'error');
                    }
                },
                (error) => { }
            );
        } catch (e) { }
    }
    selectedIndexChange(index: number) {
        setTimeout(() => this.selectedTabIndex = index);
        if (index === 0) {
            this.language_id = 1;
            this.view_categories(this.language_id);

          }
        if (index === 1) {
            this.language_id = 2;
            this.view_categories(this.language_id);
        }
        if (index === 2) {
          console.log('Telugu');
          this.language_id = 3;
          this.view_categories(this.language_id);

        }
        if (index === 3) {
          console.log('Hindi');
          this.language_id = 4;
          this.view_categories(this.language_id);
        }
        if (index === 4) {
            console.log('Hindi');
            this.language_id = 5;
            this.view_categories(this.language_id);
          }
      }
      List_All_Languages(data) {
        try {
            this.appService.postMethod('List_All_Languages', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.languageData = resp.data;
                        console.log(this.languageData, );
                    } else {
                        swal.fire(resp.msg, 'error');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }
    get_category_titles(data) {
        try {
            this.appService.getMethod('get_category_titles').subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.categoryTitle = resp.data;
                        console.log(this.categoryTitle);
                    } else {
                        swal.fire(resp.msg, 'error');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }




    view_categories(Language_ID) {
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            language_id: Language_ID,
            status: true,
        };
        try {
            this.appService.postMethod('List_All_Categories', data).subscribe(
                (resp) => {
                    if (resp.success) {
                        console.log(resp);
                        this.categoryDataNew = resp.data;
                    } else {
                        swal.fire(resp.msg, 'error');
                    }
                },
                (error) => { }
            );
        } catch (e) { }
    }


    addForm() {
        console.log(this.categoryTitle, 'Data');
        console.log(this.categoryDropdown, 'Create Category');
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            language_id: this.categoryData.language_id,
            name: this.categoryData.name,
            title: this.categoryDropdown.title,
            category_id: this.categoryDropdown.id,
            type: this.categoryDropdown.type,

        };
        console.log(data);
      try {
          this.appService.postMethod('Add_Category', data).subscribe(
              (resp: any) => {
                  if (resp.success) {
                      swal.fire(resp.msg, 'Success');
                      this.modalService.dismissAll();
                  } else {
                      swal.fire(resp.msg, 'Error');
                  }
              },
              (error) => {}
          );
      } catch (e) {}

    }



    editForm() {
        console.log(this.category_new_id, 'Edit Category');
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            language_id: this.category_new_id.language_id,
            name: this.category_new_id.name,
            category_id: this.category_new_id.id,

        };
        console.log(data);
        try {
            this.appService.postMethod('Edit_Category', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        swal.fire(resp.msg, 'Success');
                        this.modalService.dismissAll();
                      } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }


    delForm() {
        console.log('Category Deleted!!!');
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            language_id: this.language_id_data,
            category_id: this.delete_category_data.category_id,
            status: false,

        };
        console.log(data);
        try {
            this.appService.postMethod('Active_Inactive_Category', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this. delete_category = resp.msg;
                        console.log('Deleted');
                        this.modalService.dismissAll();
                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }


    open(content, data) {
        this.category_new_id = data,
        this.categoryData.category_id = this.categoryData.category_id;
        this.List_tll_Languages(this.paramsData);
        this.get_category_titles(this.paramsData);
      this.popupCalls(content);
  }
  open_category(content, data) {
    this.addModel = true;
    this.category_new_id = data,
  this.popupCalls(content);

  }
  del(content, data, language) {
    this.delete_category_data = data,
    this.language_id_data = language,
    console.log(this.delete_category_data);

    this.popupCalls(content);
}
  popupCalls(content) {
      console.log('Modal Opening', content);
      this.modalService.open(content).result.then(
          (result) => {
              this.closeResult = `Closed with: ${result}`;
              console.log('in close', content);
          },
          (reason) => {
              console.log('in dismis');
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          }
      );
  }

  private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
      } else {
          return `with: ${reason}`;
      }
  }

}
