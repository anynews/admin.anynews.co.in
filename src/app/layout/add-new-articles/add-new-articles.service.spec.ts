import { TestBed } from '@angular/core/testing';

import { AddNewArticlesService } from './add-new-articles.service';

describe('AddNewArticlesService', () => {
  let service: AddNewArticlesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddNewArticlesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
