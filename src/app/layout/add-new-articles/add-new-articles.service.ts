import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/internal/Subject';

@Injectable({
  providedIn: 'root'
})
export class AddNewArticlesService {
  subject = new Subject<any>();
  name: any;
  constructor() {
   }
   sendMessage(message: string) {
    this.subject.next({ text: message });
    this.name = message
    console.log(message,"Deepika")
}

   getMessage(): Observable<any> {
    return this.subject.asObservable();
}

}

