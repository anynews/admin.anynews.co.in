import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { HttpRequest, HttpResponse } from '@angular/common/http';
import {AddNewArticlesService} from '../add-new-articles/add-new-articles.service';
import {
  NgbModal,
  NgbModalConfig,
  ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  ValidatorFn,
} from '@angular/forms';
import { formatDate } from '@angular/common';
import { PageEvent } from '@angular/material/paginator';
@Component({
  selector: 'app-add-new-articles',
  templateUrl: './add-new-articles.component.html',
  styleUrls: ['./add-new-articles.component.scss'],
  providers: [NgbModalConfig, NgbModal],

})
export class AddNewArticlesComponent implements OnInit {
  AddArticle: FormGroup;
  addArticleData: any = {};
  adminData: any = {};
  categoriesData: any = {};
  language_id_permission = '';
  myModel = '';
  title = 'appBootstrap';
  model;
  count = 0;
  closeResult: string;
  CommentForm: FormGroup;
  EditForm: FormGroup;
  DeleteForm: FormGroup;
  progress: number;
  infoMessage: any;
  isUploading = false;
  file: File;
  isFinalImage = false;
  imageUrl: string | ArrayBuffer =
    'https://bulma.io/images/placeholders/480x480.png';
  fileName = 'No file selected';
  imageChangedEvent: any;
  croppedImage: any = '';
  previewImage: any = '';
  EditImage: any = '';

  ImageData = [];
  ImageArray = [];
  dateFilters: any = {};
  minDate = new Date();
  approveskip = 0
  limit = 10
  skip = 0
  ApprovedArticlesList = []
  selectedTabIndex = 0;
  LanguageList = [];
  uploading_type: any = '';
  youtube_link = '';
  videoData = [];
  videoArray = [];
  is_video_upload: Boolean = false;
  selectedFile: File = null;
  GoodMorning = [];
  GoodAfternoon = [];
  GoodEvening = [];
  GoodNight = [];
  morning: any = {};
  afternoon: any = {};
  evening: any = {};
  night: any = {};
  morning_01 = '';
  afternoon_01 = '';
  evening_01 = '';
  night_01 = '';
  greetingsArray = [];
  a = false;
  b = false;
  c = false;
  d = false;
  background_image = [];
  filter: any = {};
  activateFilterButton = false;
  pageEvent: PageEvent;
  datasource: null;
  pageIndex: number;
  pageSize: number;
  length: number;
  imgURL = '';
  currentPage_approve: any = {};
  onEditOpen: Boolean = false;
  post_id = 0;
  imgData = '';
  MultiLanguage: Boolean = false;
  Magazine: Boolean = false;
  LanguageData = [];
  isAdmin: Boolean = false;
  submit: Boolean = false;
  clicked: Boolean = false;
  reader = new FileReader();
  constructor(
    private appService: AppService,
    private cookieService: CookieService,
    private _appService: AppService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private router: Router,
    private dataRoute: ActivatedRoute,
    private service:AddNewArticlesService

  ) {
    this.adminData = JSON.parse(this.cookieService.get('adminData'));
    this.isAdmin = this.adminData.is_admin;
    if (this.isAdmin) {
      this.MultiLanguage = true;
      this.Magazine = true;
    } else {
      this.MultiLanguage = this.adminData.permissions.MultiLanguage;
      this.Magazine = this.adminData.permissions.Magazine;
    }
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras as {
      title: string,
      post_id: BigInteger
    };
    if (state.post_id) {
      this.onEditOpen = true;
      if (this.onEditOpen) {
        this.activateEdit(state);
      }
    }
  }
  ngOnInit(): void {
    if (!this.onEditOpen) {
      this.addArticleData.type = 'images';
    }
    this.getCategoriesData();
    this.getLanguages();
  }
  sendMessage(): void {
    // send message to subscribers via observable subject
this.service.sendMessage('employee_articles');   
}

  create() {
    this.clicked = false;
    this.submit = true;
    let body = {};
    let url = '';
    if (this.onEditOpen === true) {
      url = 'Edit_Article_News';

      if (this.addArticleData.post_type === 'normal_image' || this.addArticleData.post_type === 'normal_video') {
        if (this.addArticleData.description.length < 150) {
          swal.fire('Description should be greater than 150 Characters');
        } else {
          body = {
            post_id: this.post_id,
            admin_id: this.adminData.id,
            session_id: this.adminData.session_id,
            language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
            post_type: this.addArticleData.post_type,
            media_type: this.addArticleData.post_type === 'normal_image' ? '1' : '4',
            images: this.addArticleData.post_type === 'normal_image' ? this.ImageData : [],
            videos: this.addArticleData.post_type === 'normal_video' ? [this.addArticleData.youtube_link] : [],
            title: this.addArticleData.title,
            description: this.addArticleData.description,
            category_id: this.addArticleData.category_id,
          };
        }
      }
      if (this.addArticleData.post_type === 'full_image' || this.addArticleData.post_type === 'full_video') {
        body = {
          post_id: this.post_id,
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
          post_type: this.addArticleData.post_type,
          media_type: this.addArticleData.post_type === 'full_image' ? 1 : this.addArticleData.video_type === 'youtube_full' ? '4' : this.addArticleData.video_type === 'normal_full_video' ? '3' : '',
          images: this.addArticleData.post_type === 'full_image' ? this.ImageData : [],
          videos: this.addArticleData.post_type === 'full_image' ? [] : this.addArticleData.post_type === 'full_video' ? this.addArticleData.video_type === 'youtube_full' ? this.addArticleData.youtube_link : this.videoData : [],
          title: this.addArticleData.title,
        };
      }

      if (this.addArticleData.post_type === 'image_gallery') {
        body = {
          post_id: this.post_id,
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
          post_type: this.addArticleData.post_type,
          media_type: '2',
          images: this.addArticleData.post_type === 'image_gallery' ? this.ImageArray : [],
          videos: [],
          title: this.addArticleData.title,
        };
      }

      if (this.addArticleData.type === 'magazine') {
        body = {
          post_id: this.post_id,
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
          is_scheduled: this.addArticleData.is_scheduled ? true : false,
          published_time: this.addArticleData.published_time,
          post_type: 'magazine',
          media_type: '2',
          images: this.addArticleData.type === 'magazine' ? this.ImageArray : [],
          videos: [],
          background_image: this.background_image[0],
          title: this.addArticleData.title,
        };
      }
      if (this.addArticleData.post_type === 'greetings') {
        body = {
          post_id: this.post_id,
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
          images: [],
          videos: [],
          post_type: this.addArticleData.post_type,
          media_type: '1',
          morning: this.GoodMorning ? this.GoodMorning[0] : '',
          afternoon: this.GoodAfternoon ? this.GoodAfternoon[0] : '',
          evening: this.GoodEvening ? this.GoodEvening[0] : '',
          night: this.GoodNight ? this.GoodNight[0] : ''
        };
      }
      if (this.addArticleData.post_type === 'video_gallery') {
        body = {
          post_id: this.post_id,
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
          post_type: this.addArticleData.post_type,
          media_type: '5',
          images: [],
          videos: this.addArticleData.post_type === 'video_gallery' ? this.videoArray : [],
          title: this.addArticleData.title,
        };
      }
    }
  
    else {
      url = 'Add_Article_News'
      // language_name
      if (this.addArticleData.post_type === 'normal_image' || this.addArticleData.post_type === 'normal_video') {
        if (this.addArticleData.description.length < 150) {
          swal.fire('Description should be greater than 150 Characters');
        } else {
          body = {
            admin_id: this.adminData.id,
            session_id: this.adminData.session_id,
            language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
            is_scheduled: this.addArticleData.is_scheduled ? true : false,
            published_time: this.addArticleData.published_time,
            post_type: this.addArticleData.post_type,
            media_type: this.addArticleData.post_type === 'normal_image' ? '1' : '4',
            images: this.addArticleData.post_type === 'normal_image' ? this.ImageData : [],
            videos: this.addArticleData.post_type === 'normal_video' ? [this.addArticleData.youtube_link] : [],
            title: this.addArticleData.title,
            description: this.addArticleData.description,
            category_id: this.addArticleData.category_id,
          };
        }
      }
      if (this.addArticleData.post_type === 'full_image' || this.addArticleData.post_type === 'full_video') {
        body = {
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
          is_scheduled: this.addArticleData.is_scheduled ? true : false,
          published_time: this.addArticleData.published_time,
          post_type: this.addArticleData.post_type,
          media_type: this.addArticleData.post_type === 'full_image' ? '1' : this.addArticleData.video_type === 'youtube_full' ? '4' : this.addArticleData.video_type === 'normal_full_video' ? '3' : '',
          images: this.addArticleData.post_type === 'full_image' ? this.ImageData : [],
          videos: this.addArticleData.post_type === 'full_image' ? [] : this.addArticleData.post_type === 'full_video' ? this.addArticleData.video_type === 'youtube_full' ? [this.addArticleData.youtube_link] : this.videoData : [],
          title: this.addArticleData.title,
        };
      }
      if (this.addArticleData.post_type === 'image_gallery') {
        body = {
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
          is_scheduled: this.addArticleData.is_scheduled ? true : false,
          published_time: this.addArticleData.published_time,
          post_type: this.addArticleData.post_type,
          media_type: '2',
          images: this.addArticleData.post_type === 'image_gallery' ? this.ImageArray : [],
          videos: [],
          title: this.addArticleData.title,
        };
      }
      if (this.addArticleData.type === 'magazine') {
        body = {
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
          is_scheduled: this.addArticleData.is_scheduled ? true : false,
          published_time: this.addArticleData.published_time,
          post_type: this.addArticleData.type,
          media_type: '2',
          images: this.addArticleData.type === 'magazine' ? this.ImageArray : [],
          videos: [],
          background_image: this.background_image[0],
          title: this.addArticleData.title,
        };
      }

      if (this.addArticleData.post_type === 'video_gallery') {
        body = {
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
          is_scheduled: this.addArticleData.is_scheduled ? true : false,
          published_time: this.addArticleData.published_time,
          post_type: this.addArticleData.post_type,
          media_type: '5',
          images: [],
          videos: this.addArticleData.post_type === 'video_gallery' ? this.videoArray : [],
          title: this.addArticleData.title,
        };
      }
      if (this.addArticleData.post_type === 'greetings') {
        body = {
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.MultiLanguage ? this.addArticleData.language_id : this.adminData.language_id,
          is_scheduled: this.addArticleData.is_scheduled ? true : false,
          images: [],
          videos: [],
          published_time: this.addArticleData.published_time,
          post_type: this.addArticleData.post_type,
          media_type: '1',
          morning: this.GoodMorning ? this.GoodMorning[0] : '',
          afternoon: this.GoodAfternoon ? this.GoodAfternoon[0] : '',
          evening: this.GoodEvening ? this.GoodEvening[0] : '',
          night: this.GoodNight ? this.GoodNight[0] : ''
        };
      }
    }
    console.log(body);
    try {

      this._appService.postMethod(url, body)
        .subscribe(resp => {
          if (resp.success) {
            this.submit = false;
            this.clicked = true;
            this.addArticleData = {};
            this.addArticleData.type = 'images';
            this.ImageArray = [];
            if (this.onEditOpen) {

              swal.fire('Article Edited Succesfully');
               this.router.navigate(['/add-article']);
            } else {
              this.sendMessage();
              swal.fire('Article Added Succesfully');
            }
          } else {
            this.submit = false;
            this.clicked = false;
            swal.fire(resp.msg, 'Error');
          }
        },
          error => {
          });
    } catch (e) { }
  }
  getCategoriesData() {
    const body = {
      admin_id: this.adminData.id,
      session_id: this.adminData.session_id,
      language_id: this.adminData.language_id,
      status: 'true'
    };
    try {
      this.appService.postMethod('List_All_Categories', body).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.categoriesData = resp.data;
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  activateEdit(data) {
    this.post_id = data.post_id;
    if (data.post_type === 'normal_image' || data.post_type === 'full_image' || data.post_type === 'image_gallery' || data.post_type === 'greetings') {
      this.addArticleData.type = 'images';
      this.addArticleData.post_type = data.post_type;
    }
    if (data.post_type === 'normal_video' || data.post_type === 'full_video' || data.post_type === 'video_gallery') {
      this.addArticleData.type = 'videos';
      this.addArticleData.post_type = data.post_type;
    }
    if (data.post_type === 'normal_image' || data.post_type === 'normal_video') {
      this.addArticleData.post_type = data.post_type;
      this.addArticleData.title = data.title
      this.addArticleData.description = data.description
      this.addArticleData.category_id = data.category_id
      if (data.post_type == 'normal_image') {
        this.addArticleData.type = 'images'
        this.ImageData = data.images
      } else {
        this.addArticleData.youtube_link = data.videos[0];
      }
    }
    if (data.post_type === 'full_image' || data.post_type === 'full_video') {

      this.addArticleData.title = data.title;
      this.addArticleData.category_id = data.category_id;

      if (data.media_type === '4') {
        this.addArticleData.video_type = 'youtube_full';
        this.addArticleData.youtube_link = data.videos[0];
      }
      if (data.media_type === '3') {
        this.addArticleData.video_type = 'normal_full_video';
        this.videoData = data.videos;
      }
      if (this.addArticleData.post_type === 'full_image') {
        this.imageUrl = data.images[0];
      }
    }
    if (data.post_type === 'image_gallery') {
      this.addArticleData.post_type = data.post_type;
      this.addArticleData.title = data.title;
      this.ImageArray = data.images;
    }
    if (data.post_type === 'magazine') {
      this.addArticleData.type = 'magazine';
      this.addArticleData.title = data.title;
      this.background_image = [data.background_image];
      this.ImageArray = data.images;
      this.addArticleData.category_id = data.category_id;
    }

    if (data.post_type === 'video_gallery') {
      this.addArticleData.type = 'videos';
      this.addArticleData.post_type = data.post_type;
      this.addArticleData.title = data.title;
      this.videoArray = data.videos;
    }


    if (data.post_type === 'greetings') {
      this.addArticleData.post_type = data.post_type;
      this.GoodMorning[0] = data.morning;
      this.GoodAfternoon[0] = data.afternoon;
      this.GoodEvening[0] = data.evening;
      this.GoodNight[0] = data.night;
    }

    if (data.post_type === 'video_gallery') {
      this.addArticleData.post_type = data.post_type;

     
    }


  }

  onUpload() {
    this.infoMessage = null;
    this.progress = 0;
    this.isUploading = true;
  }

  fileChangeEvent(event: any, upload_type): void {
    this.imageChangedEvent = event;
    this.selectedFile = <File>event.target.files[0];

    if (event.target.files && event.target.files[0]) {

      this.reader.onload = (event: any) => {

        this.imgURL = event.target.result;
      };
      this.reader.readAsDataURL(event.target.files[0]);
    }


    this.uploading_type = upload_type;
    if (event.target.value) {
      this.is_video_upload = true;
    }
    if (this.uploading_type === 'morning') {
      this.reader.onload = (event: any) => {
        this.morning = event.target.result;
      };
      this.a = true;
    }
    if (this.uploading_type === 'afternoon') {
      this.reader.onload = (event: any) => {
        this.afternoon = event.target.result;
      };

      this.b = true;
    }
    if (this.uploading_type === 'evening') {
      this.reader.onload = (event: any) => {
        this.evening = event.target.result;
      };

      this.c = true;
    }
    if (this.uploading_type === 'night') {
      this.reader.onload = (event: any) => {
        this.night = event.target.result;
      };

      this.d = true;
    }
  }

  okFullImage() {
    this.uploadToServer(this.selectedFile);
    this.imageChangedEvent = '';
    this.croppedImage = '';
    this.morning_01 = '';
    this.afternoon_01 = '';
    this.evening_01 = '';
    this.night_01 = '';
    this.morning = [];
    this.afternoon = [];
    this.night = [];
    this.evening = [];
    this.imgURL = '';
  }
  okImage() {

    if (this.ImageData.length > 0) {
    }
    this.previewImage = this.croppedImage;
    if (this.uploading_type === 'morning') {
      this.morning_01 = this.croppedImage;
      this.a = false;
    }
    if (this.uploading_type === 'afternoon') {
      this.afternoon_01 = this.croppedImage;
      this.b = false;
    }
    if (this.uploading_type === 'evening') {
      this.evening_01 = this.croppedImage;
      this.c = false;
    }
    if (this.uploading_type === 'night') {
      this.night_01 = this.croppedImage;
      this.d = false;
    }
    let imag;
    const str: string = this.croppedImage;
    let st4;
    if (str.indexOf('data:image/jpeg;base64,') !== -1) {
      st4 = str.replace('data:image/jpeg;base64,', '');
    } else {
      st4 = str.replace('data:image/png;base64,', '');
    }
    const imageBlob = this.dataURItoBlob(st4);
    const imageFile = new File([imageBlob], 'imageName.jpeg', { type: 'image/jpeg' });
    // Once Uploaded
    this.uploadToServer(imageFile);
    this.imageChangedEvent = '';
    this.croppedImage = '';
    this.morning_01 = '';
    this.afternoon_01 = '';
    this.evening_01 = '';
    this.night_01 = '';
    this.morning = [];
    this.afternoon = [];
    this.night = [];
    this.evening = [];
  }

  okvideo() {
    this.uploadToServer(this.selectedFile);

  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // this.showCropper = true;
    // show cropper
  }

  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  onChange(event) {
    const format = 'yyyy/MM/dd';
    const formattedDate = formatDate(event, format, 'en-US');
    this.getEmployeeArticleList(0, formattedDate);
  }

  onRemoveImage(removeIndex) {
    this.ImageArray.splice(removeIndex, 1);
    swal.fire('Removed Successfully');
  }

  onRemoveVideo(index) {
    this.videoArray.splice(index, 1);
    swal.fire('Removed Successfully');
  }

  getLanguages() {
    const adminData: any = JSON.parse(this.cookieService.get('adminData'));
    const body = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
    };
    try {
      this._appService.postMethod('List_All_Languages', body)
        .subscribe(resp => {
          if (resp.success) {
            this.LanguageData = resp.data;
          } else {
          }
        },
          error => {

          });
    } catch (e) { }
  }
  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }
  uploadToServer(imageFile) {
    const formData = new FormData();
    let url = '';
    let key = '';
    url = 'Upload_File';
    key = 'image';
    formData.append(key, imageFile);
    const req = new HttpRequest('POST', this._appService.Upload_Url + url, formData, {
      reportProgress: true,
      withCredentials: false
    });
    this._appService.onUploadFile(req)
      .subscribe(
        (event: any) => {
          if (event instanceof HttpResponse) {
            this.isFinalImage = true;
            if (this.uploading_type === 1) {
              this.croppedImage = '';
              this.ImageData = [];
              this.ImageData.push(event.body.extras.url);
            } else if (this.uploading_type === 2) {
              this.croppedImage = '';
              this.ImageArray.push(event.body.extras.url);
            } else if (this.uploading_type === 4) {
              this.videoData = [];
              this.videoData.push(event.body.extras.url);
            } else if (this.uploading_type === 5) {
              this.videoArray.push(event.body.extras.url);
            } else if (this.uploading_type === 'morning') {
              this.GoodMorning = [];
              this.GoodMorning.push(event.body.extras.url);
              this.greetingsArray.push(event.body.extras.url);
            } else if (this.uploading_type === 'afternoon') {
              this.GoodAfternoon = [];
              this.GoodAfternoon.push(event.body.extras.url);
              this.greetingsArray.push(event.body.extras.url);
            } else if (this.uploading_type === 'evening') {
              this.GoodEvening = [];
              this.GoodEvening.push(event.body.extras.url);
              this.greetingsArray.push(event.body.extras.url);
            } else if (this.uploading_type === 'night') {
              this.GoodNight = [];
              this.GoodNight.push(event.body.extras.url);
              this.greetingsArray.push(event.body.extras.url);
            } else if (this.uploading_type === 'theme') {
              this.background_image = [];
              this.background_image.push(event.body.extras.url);
            }

            swal.fire("Image Uploaded Succesfully");
          
          } else if (event instanceof HttpResponse) { }
        },
        err => {
        }
      );
  }
  changeComboo(event) {
    if (event) {
      this.imgURL = '';
      this.croppedImage = '';
      this.ImageData = [];
      this.addArticleData.image = '';
      this.previewImage = '';
      this.addArticleData.title = '';
      this.addArticleData.description = '';
      this.addArticleData.is_scheduled = '';
      this.addArticleData.category_id = '';
      this.imageChangedEvent = '';
      this.imageUrl = 'https://bulma.io/images/placeholders/480x480.png';
      this.isFinalImage = false;
    }
  }
  applyfilter() {
    this.getEmployeeArticleList(0, '');
  }



  getEmployeeArticleList(skip, date) {
    const body = {
      admin_id: this.adminData.id,
      session_id: this.adminData.session_id,
      skip: skip,
      limit: this.limit,
      status: 1,
      start_time: date,
      category_id: this.activateFilterButton ? this.filter.category_id : 0
    };
    try {

      this._appService.postMethod('List_Employee_Articles', body)
        .subscribe(resp => {
          if (resp.success) {
            if (this.approveskip == 0) {


            } else {

            }
            this.count = resp.Count;
            this.ApprovedArticlesList = resp.data;
            this.datasource = resp.data;
            this.length = resp.count;


          } else {



          }
        },
          error => {

          });
    } catch (e) { }
  }



  onNextPage(event) {
    this.currentPage_approve = event;
    this.approveskip = event.pageIndex * 10;
    this.getEmployeeArticleList(this.approveskip, '');
    return event;
  }

  activateFilter() {
    if (this.activateFilterButton) {
      this.activateFilterButton = false;
    } else {
      this.activateFilterButton = true;
    }

  }
  getLanguageList() {
    const body = {
      admin_id: this.adminData.id,
      session_id: this.adminData.session_id,
      language_id: this.adminData.language_id,
      skip: this.approveskip,
      limit: this.limit,
      status: 1,
    };
    try {
      this._appService.postMethod('List_Language_Articles', body)
        .subscribe(resp => {
          if (resp.success) {
            this.LanguageList = resp.data;


          } else {
          }
        },
          error => {
          });
    } catch (e) { }
  }
  selectedIndexChange(index: number) {
    setTimeout(() => this.selectedTabIndex = index);
    if (index === 0) {
    }
    if (index === 1) {
      this.getEmployeeArticleList(0, '');
    }
    if (index === 2) {
      this.getLanguageList();
    }
    if (index === 3) {
      this.getLanguageList();
    }
  }
  delForm() {
  }
  editForm() {
  }
  comForm() {
  }

  open(content) {
    this.popupCalls(content);

  }
  com(content) {
    this.popupCalls(content);

  }
  del(content) {


    this.popupCalls(content);
  }
  popupCalls(content) {
    this.modalService.open(content).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
