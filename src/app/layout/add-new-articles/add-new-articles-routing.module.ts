import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddNewArticlesComponent} from './add-new-articles.component'

const routes: Routes = [
  {
    path: '',
    component: AddNewArticlesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddNewArticlesRoutingModule { }
