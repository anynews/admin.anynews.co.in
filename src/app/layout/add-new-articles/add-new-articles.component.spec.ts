import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewArticlesComponent } from './add-new-articles.component';

describe('AddNewArticlesComponent', () => {
  let component: AddNewArticlesComponent;
  let fixture: ComponentFixture<AddNewArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
