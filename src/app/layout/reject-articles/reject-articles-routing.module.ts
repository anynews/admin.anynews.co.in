import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RejectArticlesComponent } from './reject-articles.component';


const routes: Routes = [
  {
    path: '',
    component: RejectArticlesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RejectArticlesRoutingModule { }
