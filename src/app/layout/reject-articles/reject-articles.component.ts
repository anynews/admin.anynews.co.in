import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-reject-articles',
  templateUrl: './reject-articles.component.html',
  styleUrls: ['./reject-articles.component.scss']
})
export class RejectArticlesComponent implements OnInit {

  selectedTabIndex = 0
  activateFilterButton: Boolean = false;
  dateFilters: any = {}
  minDate = new Date();
  userData = []
  filter : any ={}
  paramsData : any = {}
  adminData: any = {}
  districts = []
  formattedDate : String = ''
  selected = ''
  userarticleData : any = {}
  image = ''
  isReject: boolean = false;
  toggle = true;
status = 'Enable'; 
currentPage_approve: any = {}
approveskip = 0
pageIndex: number;
pageSize: number;
length: number;
reporterlength : number;
pageEvent: PageEvent;
data : Boolean = false;
  constructor(
    private cookieService: CookieService,
    private appService: AppService,
  ) {
    this.adminData = JSON.parse(this.cookieService.get('adminData'));
    this.selected = '0'
   }

  ngOnInit(): void {
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
    this.view_districts(this.adminData.is_admin?0:this.adminData.language_id);
    this.get_users_articles();
    this.filter.published_status = '0'
  }

  selectedIndexChange(index: number) {
    setTimeout(() => this.selectedTabIndex = index);
    if (index === 0) {
    }
    if (index === 1) {
    }
  }

  publishStatus(value){
    this.get_users_articles()
    console.log("Publish Status")
  }

  district(value){
    this.get_users_articles()
    console.log("District Status")

  }

  onChange(event) {
    const format = 'yyyy-MM-dd';
    this.formattedDate = formatDate(event, format, 'en-US');
    console.log("Date Status")
    this.get_users_articles()

  }

  get_users_articles() {
    const data = {
        admin_id: this.paramsData.admin_id,
        session_id: this.paramsData.session_id,
        status : 2,
        language_id : this.adminData.is_admin?0:this.adminData.language_id,
        start_time: this.formattedDate,
        dist_id: this.filter.district_id,
        skip: this.approveskip
    };
    console.log(data);
    try {
      this.appService.postMethod('get_users_articles', data).subscribe(
          (resp: any) => {
              if (resp.success) {
                  this.userData = resp.data;
                  this.length = resp.count;

                  const format = 'dd MMM hh:mm a'
                  for (let index = 0; index < this.userData.length; index++) {
                    let hello = this.userData[index];
                    const formattedDate = formatDate(hello.createdAt, format, 'en-US');
                    hello.createdAt = formattedDate;
                    console.log(hello, "Updated Data")
                  }
                  if(this.userData[0]!=null){
                    this.data= true
                    this.userarticleData = this.userData[0]
                    this.image = this.userarticleData.images[0];
                    console.log(this.userData,"Deepika");
                  }else{
                    this.data = false;
                  }
              } else {
                  swal.fire(resp.msg, '');
              }
          },
          (error) => {}
      );
  } catch (e) {}
}


  view_districts(state) {
    const data = {
        admin_id: this.paramsData.admin_id,
        session_id: this.paramsData.session_id,
        language_id: state,
    };
    console.log(data);
    try {
        this.appService.postMethod('view_districts', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.districts = resp.data;
                    console.log(this.districts,"Deepika");
                } else {
                    swal.fire(resp.msg, '');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}

getdata(data){
  this.isReject = false;
  this.userarticleData = {}
  console.log(this.userarticleData,"Empty")
  this.userarticleData = data
  console.log(this.userarticleData,"Data")
  this.image = this.userarticleData.images[0];
  console.log(this.userarticleData.images[0],"Image")
}
reasons(){
  this.isReject = true;
}

edit_user_post() {
  const data = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      user_post_id: this.userarticleData.user_post_id,
      title: this.userarticleData.title,
      description: this.userarticleData.description
  };
  console.log(data);
  try {
      this.appService.postMethod('edit_user_post', data).subscribe(
          (resp: any) => {
              if (resp.success) {
                swal.fire(resp.msg, '');
              } else {
                  swal.fire(resp.msg, '');
              }
          },
          (error) => {}
      );
  } catch (e) {}
}

pullback_article() {
  const data = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      user_post_id: this.userarticleData.user_post_id,
      reason: this.userarticleData.reason_id
      
     
  };
  try {
      this.appService.postMethod('pullback_article', data).subscribe(
          (resp: any) => {
              if (resp.success) {
                swal.fire(resp.msg, '');
              } else {
                  swal.fire(resp.msg, '');
              }
          },
          (error) => {}
      );
  } catch (e) {}
}
onNextPage(event) {
  console.log(event)
  this.currentPage_approve = event
  this.approveskip = event.pageIndex * 10
  console.log(this.approveskip, "Skip")
  this.get_users_articles();
  return event
}
}
