import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectArticlesComponent } from './reject-articles.component';

describe('RejectArticlesComponent', () => {
  let component: RejectArticlesComponent;
  let fixture: ComponentFixture<RejectArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
