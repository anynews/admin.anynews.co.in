import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RejectArticlesRoutingModule } from './reject-articles-routing.module';
import { RejectArticlesComponent } from './reject-articles.component';
import { MatInputModule } from '@angular/material/input';
// import {MatTabsModule} from '@angular/material/tabs';
// import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import { ReactiveFormsModule ,FormsModule} from '@angular/forms';
// import {MatButtonToggleModule} from '@angular/material/button-toggle';
// import {MatRadioModule} from '@angular/material/radio';
// import { ImageCropperModule } from 'ngx-image-cropper';
// import { CarouselModule } from 'ngx-bootstrap/carousel';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';

@NgModule({
  declarations: [RejectArticlesComponent],
  imports: [
    CommonModule,
    RejectArticlesRoutingModule,
    CommonModule,
    // MatTabsModule,
    FormsModule,
    MatIconModule,
    MatInputModule,
    // MatSlideToggleModule,
    MatSelectModule,
    MatCardModule,
    ReactiveFormsModule,
    // MatButtonToggleModule,
    // MatRadioModule,
    // ImageCropperModule,
    // CarouselModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule

  ]
})
export class RejectArticlesModule { }
