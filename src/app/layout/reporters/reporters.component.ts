import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import {  NgbModal,  NgbModalConfig,  ModalDismissReasons,} from '@ng-bootstrap/ng-bootstrap';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-reporters',
  templateUrl: './reporters.component.html',
  styleUrls: ['./reporters.component.scss']
})
export class ReportersComponent implements OnInit {

  hide = true;
  registrationData: any = {};
  permissionsData: any = {};
  adminData: any;
  RegistrationForm: FormGroup;
  isShown = false;
  showMe = true;
  optionValue = '';
  optionalValue = '';
  Telengana = '';
  is_admin = '';
  isAdminListLoadin: boolean;
  LanguageList: [];
  userData: []
  reportersData: []
  singleUserArticles : []
  selectedTabIndex = 0
  closeResult: string;
  currentPage_approve: any = {}
  approveskip = 0
  pageIndex: number;
  pageSize: number;
  length: number;
  reporterlength : number;
  pageEvent: PageEvent;
  constructor(
    private router: Router,
    private appService: AppService,
    private cookieService: CookieService,
    private _appService: AppService,
    private modalService: NgbModal,
  ) {
    this.getLanguageList();
  }
  ngOnInit() {
    this.get_users();
  }
  getLanguageList() {
    this.isAdminListLoadin = true;
    const adminData = JSON.parse(this.cookieService.get('adminData'));
    const body = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
    };
    try {
      this._appService.postMethod('List_All_Languages', body)
        .subscribe(resp => {
          if (resp.success) {
            this.isAdminListLoadin = false;
            this.LanguageList = resp.data;
          } else {
            this.isAdminListLoadin = false;
            swal.fire('Enter All Tags', 'Something went wrong!', 'error');
          }
        }, error => {
        });
    } catch (e) { }
  }
  get_users() {
    const adminData = JSON.parse(this.cookieService.get('adminData'));
    const data = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      skip: this.approveskip
    };
    try {
      this.appService.postMethod('get_users', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.userData = resp.data;
            this.length = resp.count
          } else {
            swal.fire(resp.msg, '');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  get_reporters() {
    const adminData = JSON.parse(this.cookieService.get('adminData'));
    const data = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      skip : this.approveskip
    };
    try {
      this.appService.postMethod('get_reporters', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.reportersData = resp.data;
            this.reporterlength = resp.count;
          } else {
            swal.fire(resp.msg, '');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  update_user_status(user_data,status){
    console.log("Hello")
    const adminData = JSON.parse(this.cookieService.get('adminData'));
    const data = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      user_id : user_data.id,
      status : status
    };
    try {
      this.appService.postMethod('update_user_status', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            swal.fire(resp.msg, '');
            this.get_users();
          } else {
            swal.fire(resp.msg, '');
            this.get_users();
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  com(content, data) {
    this.popupCalls(content);
    this.get_user_articles(data);
  }
  article(content, data) {
    this.popupCalls(content);
    this.get_user_articles_reporter(data);
  }
  popupCalls(content) {
    console.log('Modal Opening', content);
    this.modalService.open(content).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
        console.log('in close', content);
      },
      (reason) => {
        console.log('in dismis');
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  get_user_articles(user_data){
    const adminData = JSON.parse(this.cookieService.get('adminData'));
    const data = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      user_id : user_data.id,
    };
    try {
      this.appService.postMethod('get_user_articles', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.singleUserArticles = resp.data;
          } else {
            swal.fire(resp.msg, '');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  get_user_articles_reporter(user_data){
    const adminData = JSON.parse(this.cookieService.get('adminData'));
    const data = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      user_id : user_data.writer_id,
    };
    try {
      this.appService.postMethod('get_user_articles', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.singleUserArticles = resp.data;
          } else {
            swal.fire(resp.msg, '');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  selectedIndexChange(index: number) {
    setTimeout(() => this.selectedTabIndex = index);
    if (index === 0) {
      this.get_users();
      console.log("Users")
    }
    if (index === 1) {
      console.log("Reporsts")
      this.get_reporters();
    }
  }


  update_reporter_status(user_data){
    const adminData = JSON.parse(this.cookieService.get('adminData'));
    const data = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      writer_id : user_data.writer_id,
      status: 2
    };
    try {
      this.appService.postMethod('update_reporter_status', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.singleUserArticles = resp.data;
            swal.fire(resp.msg,'')
            this.get_reporters();
          } else {
            swal.fire(resp.msg, '');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }

  onNextPage(event) {
    console.log(event)
    this.currentPage_approve = event
    this.approveskip = event.pageIndex * 10
    console.log(this.approveskip, "Skip")
    this.get_users();
    return event
  }

  
  onNextReporterPage(event) {
    console.log(event)
    this.currentPage_approve = event
    this.approveskip = event.pageIndex * 10
    console.log(this.approveskip, "Skip")
    this.get_reporters();
    return event
  }

  block(data) {
    swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover the Reporter again',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Block the Reporter!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        swal.fire(
          'Deleted!',
          'Your Reporter blocked Sucessfully.',
          'success'
        )
        this.update_reporter_status(data);
        // For more information about handling dismissals please visit
        // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Cancelled',
          'Reporter is safe :)',
          'error'
        )
      }
    })

  }
}
