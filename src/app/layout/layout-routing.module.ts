import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },
       
            {
                path: 'categories',
                loadChildren: () => import('./categories/categories.module').then(m => m.CategoriesModule)
            },
            {
                path: 'add-new-articles',
                loadChildren: () => import('./add-new-articles/add-new-articles.module').then(m => m.AddNewArticlesModule)
            },
            {
                path: 'add-article',
                loadChildren: () => import('./add-article/add-article.module').then(m => m.AddArticleModule)
            },
            {
                path: 'languages',
                loadChildren: () => import('./languages/languages.module').then(m => m.LanguagesModule)
            },
            {
                path: 'profile',
                loadChildren: () => import('./testing/testing.module').then(m => m.TestingModule)
            },
            {
                path: 'static-data',
                loadChildren: () => import('./static-data/static-data.module').then(m => m.StaticDataModule)
            },
            {
                path: 'registration',
                loadChildren: () => import('./registration/registration.module').then(m => m.RegistrationModule)
            },
            {
                path: 'edit-employee',
                loadChildren: () => import('./edit-employee/edit-employee.module').then(m => m.EditEmployeeModule)
            },
       
            {
                path: 'locations',
                loadChildren: () => import('./locations/locations.module').then(m => m.LocationsModule)
            },
            {
                path: 'reporters',
                loadChildren: () => import('./reporters/reporters.module').then(m => m.ReportersModule)
            },
            {
                path: 'payments',
                loadChildren: () => import('./payments/payments.module').then(m => m.PaymentsModule)
            },
            {
                path: 'pending-articles',
                loadChildren: () => import('./user-articles/user-articles.module').then(m => m.UserArticlesModule)
            },
            {
                path: 'published-articles',
                loadChildren: () => import('./published-articles/published-articles.module').then(m => m.PublishedArticlesModule)
            },
            {
                path: 'rejected-articles',
                loadChildren: () => import('./reject-articles/reject-articles.module').then(m => m.RejectArticlesModule)
            },
          
            {
                path: 'pullback-articles',
                loadChildren: () => import('./pull-back/pull-back.module').then(m => m.PullBackModule)
            },


        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}
