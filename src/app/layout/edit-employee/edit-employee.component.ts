import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss'],

})
export class EditEmployeeComponent implements OnInit {
  hide = true;
  employeeData: any = {};
  registrationData: any = {};
  permissionsData: any = {};
  adminData: any;
  paramsData: any = {};
  isShown = false;
  showMe = true;
  optionValue = '';
  optionalValue = '';
  isEdit: any = {};
  is_admin = '';
  role = '';
  isAdminListLoadin: boolean;
  LanguageList: any;
  closeResult: string;
  languageData: any = {};
  employee_new_id: any = {};
  addLanguageData: any = {};
  DeleteForm: FormGroup;
  delete_employee_data: any = {};
  delete_employee: any = {};

  constructor(
    private router: Router,
    private appService: AppService,
    private cookieService: CookieService,
    private _appService: AppService,
  ) {
    this.adminData = JSON.parse(this.cookieService.get('adminData'));
  }
  roles = {

  };

  ngOnInit(): void {
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
    this.List_All_Languages(this.paramsData);
    this.List_All_Employees(0);
  }

  List_All_Languages(data) {
    try {
        this.appService.postMethod('List_All_Languages', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.languageData = resp.data;
                    // console.log(resp);
                } else {
                    swal.fire(resp.msg, 'error');
                }
            },
            (error) => { }
        );
    } catch (e) { }
}
  List_All_Employees(skip) {
    console.log(this.employeeData);
    const data = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      skip: skip,
      limit: 10,
    };
    try {
        this.appService.postMethod('List_All_Employees', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.employeeData = resp.data;

                    console.log(resp);
                } else {
                    swal.fire(resp.msg, 'error');
                }
            },
            (error) => { }
        );
    } catch (e) { }
}
editForm() {
  const data = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      language_id: this.employee_new_id.language_id,
      district_id: this.employee_new_id.id,
      name: this.registrationData.name,

      // category_wise_titles: this.categoryData.category_wise_title,
  };
  console.log(data);
  try {
      this.appService.postMethod('edit_employee', data).subscribe(
          (resp: any) => {
              if (resp.success) {
                  this.employeeData = resp.msg;
                  console.log(resp);
              } else {
                  swal.fire(resp.msg, '');
              }
          },
          (error) => {}
      );
  } catch (e) {}
}


}
