import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditEmployeeRoutingModule } from './edit-employee-routing.module';
import { EditEmployeeComponent } from './edit-employee.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import { ReactiveFormsModule ,FormsModule} from '@angular/forms';


@NgModule({
  declarations: [EditEmployeeComponent],
  imports: [
    CommonModule,
    EditEmployeeRoutingModule,
    FlexLayoutModule,
    FormsModule,
    MatIconModule,
    MatInputModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatCardModule,
    ReactiveFormsModule,
  ]
})
export class EditEmployeeModule { }
