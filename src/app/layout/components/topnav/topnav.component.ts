import { Component, OnInit } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { MatDialog } from "@angular/material/dialog";
import { timer, Subscription } from "rxjs";
import * as moment from "moment";
import { Injectable } from "@angular/core";
import { AddNewArticlesService } from "../../add-new-articles/add-new-articles.service";
import { UserArticlesService } from "../../user-articles/user-articles.service";
import { BehaviorSubject, Observable } from "rxjs";
import { CookieService } from "ngx-cookie-service";
import { AppService } from "src/app/app.service";
import swal from "sweetalert2";

@Injectable({
    providedIn: "root",
})
@Component({
    selector: "app-topnav",
    templateUrl: "./topnav.component.html",
    styleUrls: ["./topnav.component.scss"],
})
export class TopnavComponent implements OnInit {
    public pushRightClass: string;
    countDown: Subscription;
    durations: Subscription;
    counter = 1201;
    tick = 1000;
    private routerInfo: BehaviorSubject<boolean>;
    resultstring: String;
    name: any;
    name1: any;
    _subscription: Subscription;
    _subscription1: Subscription;
    adminData: any = {};
    last_event: String;
    constructor(
        public router: Router,
        private translate: TranslateService,
        dialog: MatDialog,
        private nameService: AddNewArticlesService,
        private nameService1: UserArticlesService,
        private cookieService: CookieService,
        private appService: AppService
    ) {
        if(this.cookieService.get('adminData')){
            this.adminData = JSON.parse(this.cookieService.get('adminData'));
        }
        else{
            this.adminData = {};
            this.router.navigate(["/login"]);
        }
        this.router.events.subscribe((val) => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
        this.routerInfo = new BehaviorSubject<boolean>(false);
        this.name = nameService.name;
        this._subscription = nameService.subject.subscribe((value) => {
            this.last_event = value.text;
            this.counter = 1201;
            console.log(this.last_event);
        });

        this.name1 = nameService1.name1;
        this._subscription1 = nameService1.subject1.subscribe((value) => {
            this.last_event = value.text;
            this.counter = 1201;
            console.log(this.last_event);
        });
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }

    ngOnInit() {
        this.pushRightClass = "push-right";
        this.countDown = timer(0, this.tick).subscribe(() =>
            this.timeformat(--this.counter)
        );
    }

    logout() {
        const data = {
            admin_id: this.adminData.id,
            session_id: this.adminData.session_id,
            logout_type: 1, //Automatic Logout
            last_event: this.last_event,
        };
        try {
            this.appService.postMethod("logout", data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        localStorage.removeItem("isLoggedin");
                        this.router.navigate(["/login"]);
                    } else {
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }
    
    timeformat(Counter) {
        if (Counter === 0) {
            this._subscription.unsubscribe();
            this._subscription1.unsubscribe();
            console.log("Logout");
            this.logout();
        } else {
            const duration = moment.duration(Counter, "seconds");
            this.resultstring = moment
                .utc(duration.asMilliseconds())
                .format("HH:mm:ss");
        }
    }
    isToggled(): boolean {
        const dom: Element = document.querySelector("body");
        return dom.classList.contains(this.pushRightClass);
    }
    toggleSidebar() {
        const dom: any = document.querySelector("body");
        dom.classList.toggle(this.pushRightClass);
    }
    onLoggedout() {
        const data = {
            admin_id: this.adminData.id,
            session_id: this.adminData.session_id,
            logout_type: 0, //Manual Logout
            last_event: this.last_event? this.last_event : '',
        };
        try {
            this.appService.postMethod("logout", data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        localStorage.removeItem("isLoggedin");
                        this.router.navigate(["/login"]);
                    } else {
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }
    changeLang(language: string) {
        this.translate.use(language);
    }
}
