import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable, interval, Subscription } from 'rxjs';
import { AppService } from 'src/app/app.service';
import { Router, NavigationEnd } from "@angular/router";

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
    public showMenu: string;
    newColor = false;
    adminData: any = {};
    permissions: any = {};
    is_admin: Boolean;
    count: any = {};
    showVar=true;
    private updateSubscription: Subscription;
    constructor(
        private cookieService: CookieService,
        private appService: AppService,
        public router: Router,

    ) {
        if(this.cookieService.get('adminData')){
            this.adminData = JSON.parse(this.cookieService.get('adminData'));
        }
        else{
            this.adminData = {};
            this.router.navigate(["/login"]);
        }
    }

    ngOnInit() {
        this.showMenu = '';
        console.log(this.adminData, 'Permissions Data');
        if(this.adminData.permissions){
            this.permissions = this.adminData.permissions;
        }else{
            this.permissions = {};
        }
        this.is_admin = this.adminData.is_admin;
        console.log(this.is_admin,"Admin Status")
        this.updateStats();
        this.updateSubscription = interval(60000).subscribe((val) => {
            this.updateStats();
        });
    }
    updateStats() {
        const body = {
            admin_id: this.adminData.id,
            session_id: this.adminData.session_id,
        };
        try {
            this.appService.postMethod('user_articles_count', body).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.count = resp.data;
                    } else {
                        console.log(resp.msg, 'error');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    toggleColor() {
        this.newColor = !this.newColor;
    }
    image() {
        console.log("Hllo");
        this.showVar = !this.showVar;
    }
}
