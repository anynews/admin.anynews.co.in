import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import swal from 'sweetalert2';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { HttpRequest, HttpResponse } from '@angular/common/http';
import { NgbModal, NgbModalConfig, ModalDismissReasons, } from '@ng-bootstrap/ng-bootstrap';
import { Router, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { formatDate } from '@angular/common';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class AddArticleComponent implements OnInit {
  AddArticle: FormGroup;
  addArticleData: any = {};
  adminData: any = {}
  categoriesData: any = {}
  language_id_permission = ''
  myModel = ''
  title = 'appBootstrap';
  model;
  count = 0;
  counT = 0;
  Length = 0;
  istitleVisible = false;
  modalTitleDescription: any;
  closeResult: string;
  CommentForm: FormGroup;
  EditForm: FormGroup;
  DeleteForm: FormGroup;
  ViewForm: FormGroup;
  progress: number;
  infoMessage: any;
  isUploading: boolean = false;
  file: File;
  isFinalImage: boolean = false;
  imageUrl: string | ArrayBuffer =
    "https://bulma.io/images/placeholders/480x480.png";
  fileName: string = "No file selected";
  imageChangedEvent: any;
  croppedImage: any = '';
  previewImage: any = ''
  ImageData = [];
  ImageArray = [];
  dateFilters: any = {};
  minDate = new Date();
  approveskip = 0
  limit = 10
  skip = 0
  ApprovedArticlesList = []
  selectedTabIndex = 0;
  LanguageList = []
  uploading_type: any = ''
  youtube_link = ''
  videoData = []
  videoArray = []
  is_video_upload: Boolean = false
  selectedFile: File = null;
  GoodMorning = []
  GoodAfternoon = []
  GoodEvening = []
  GoodNight = []
  morning: any = {}
  afternoon: any = {}
  evening: any = {}
  night: any = {}
  morning_01 = ''
  afternoon_01 = ''
  evening_01 = ''
  night_01 = ''
  greetingsArray = []
  a: boolean = false
  b: boolean = false
  c: boolean = false
  d: boolean = false
  background_image = []
  filter: any = {}
  activateFilterButton: boolean = false
  pageEvent: PageEvent;
  datasource: null;
  pageIndex: number;
  pageSize: number;
  length: number;
  imgURL = ''
  isEdit: any = {};
  currentPage_approve: any = {}
  state: any = {}
  title_modal = ''
  description_modal = ''
  isDescription: Boolean = false
  commentData: any = {}
  LanguageData: any = {}
  Languages = []
  language_name = ''
  constructor(
    private appService: AppService,
    private cookieService: CookieService,
    private _appService: AppService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private router: Router,
  ) {
    this.adminData = JSON.parse(this.cookieService.get('adminData'));
  }

  ngOnInit(): void {
    this.addArticleData.type = 'images'
    this.getCategoriesData();
    this.getEmployeeArticleList(0, '')
    this.getLanguages();
    this.getEmployeeLanguage(this.adminData.language_id);
  }




  EditArticle(event, data) {
    this.router.navigate(['add-new-articles'], data);
  }
  create() {
    let body = {}
    let url = ''



    url = 'Add_Article_News'
    // language_name
    if (this.addArticleData.post_type == 'normal_image' || this.addArticleData.post_type == 'normal_video') {
      if (this.addArticleData.description.length < 150) {
        swal.fire("Description Should be greater than 150 Characters")
      }
      else {
        body = {
          admin_id: this.adminData.id,
          session_id: this.adminData.session_id,
          language_id: this.adminData.language_id,
          is_scheduled: this.addArticleData.is_scheduled ? true : false,
          published_time: this.addArticleData.published_time,
          post_type: this.addArticleData.post_type,
          media_type: 1,
          images: this.addArticleData.post_type == 'normal_image' ? this.ImageData : [],
          videos: this.addArticleData.post_type == 'normal_video' ? this.addArticleData.youtube_link : [],
          title: this.addArticleData.title,
          description: this.addArticleData.description,
          category_id: this.addArticleData.category_id,
        }
      }
    }
    if (this.addArticleData.post_type == 'full_image' || this.addArticleData.post_type == 'full_video') {
      body = {
        admin_id: this.adminData.id,
        session_id: this.adminData.session_id,
        language_id: this.adminData.language_id,
        is_scheduled: this.addArticleData.is_scheduled ? true : false,
        published_time: this.addArticleData.published_time,
        post_type: this.addArticleData.post_type,
        media_type: this.addArticleData.post_type == 'full_image' ? 1 : this.addArticleData.video_type == 'youtube_full' ? 4 : this.addArticleData.video_type == 'normal_full_video' ? 3 : '',
        images: this.addArticleData.post_type == 'full_image' ? this.ImageData : [],
        videos: this.addArticleData.post_type == 'full_image' ? [] : this.addArticleData.post_type == 'full_video' ? this.addArticleData.video_type == 'youtube_full' ? this.addArticleData.youtube_link : this.videoData : [],
        title: this.addArticleData.title,
      }
    }
    if (this.addArticleData.post_type == 'image_gallery') {
      body = {
        admin_id: this.adminData.id,
        session_id: this.adminData.session_id,
        language_id: this.adminData.language_id,
        is_scheduled: this.addArticleData.is_scheduled ? true : false,
        published_time: this.addArticleData.published_time,
        post_type: this.addArticleData.post_type,
        media_type: 2,
        images: this.addArticleData.post_type == 'image_gallery' ? this.ImageArray : [],
        videos: [],
        title: this.addArticleData.title,
      }
    }
    if (this.addArticleData.type == 'magazine') {
      body = {
        admin_id: this.adminData.id,
        session_id: this.adminData.session_id,
        language_id: this.adminData.language_id,
        is_scheduled: this.addArticleData.is_scheduled ? true : false,
        published_time: this.addArticleData.published_time,
        post_type: this.addArticleData.type,
        media_type: 2,
        images: this.addArticleData.type == 'magazine' ? this.ImageArray : [],
        videos: [],
        background_image: this.background_image,
        title: this.addArticleData.title,
      }
    }

    if (this.addArticleData.post_type == 'video_gallery') {
      body = {
        admin_id: this.adminData.id,
        session_id: this.adminData.session_id,
        language_id: this.adminData.language_id,
        is_scheduled: this.addArticleData.is_scheduled ? true : false,
        published_time: this.addArticleData.published_time,
        post_type: this.addArticleData.post_type,
        media_type: 2,
        images: [],
        videos: this.addArticleData.post_type == 'video_gallery' ? this.videoArray : [],
        title: this.addArticleData.title,
      }
    }
    if (this.addArticleData.post_type == 'greetings') {
      body = {
        admin_id: this.adminData.id,
        session_id: this.adminData.session_id,
        language_id: this.adminData.language_id,
        is_scheduled: this.addArticleData.is_scheduled ? true : false,
        images: [],
        videos: [],
        published_time: this.addArticleData.published_time,
        post_type: this.addArticleData.post_type,
        media_type: 1,
        morning: this.GoodMorning ? this.GoodMorning[0] : '',
        afternoon: this.GoodAfternoon ? this.GoodAfternoon[0] : '',
        evening: this.GoodEvening ? this.GoodEvening[0] : '',
        night: this.GoodNight ? this.GoodNight[0] : ''
      }
    }
    try {

      this._appService.postMethod(url, body)
        .subscribe(resp => {
          if (resp.success) {
            this.addArticleData = {}
            this.addArticleData.type = 'images'
          }
          else {
          }
        },
          error => {

          })
    } catch (e) { }


  }

  getCategoriesData() {
    let body = {
      admin_id: this.adminData.id,
      session_id: this.adminData.session_id,
      language_id: this.adminData.language_id,
      status: 'true'
    }
    try {
      this.appService.postMethod('List_All_Categories', body).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.categoriesData = resp.data;
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }


  SendPush(data) {
    swal.fire({
      title: 'Push Notification',
      text: 'Do you want to send Push Notification?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Send!',
      cancelButtonText: 'No!'
    }).then((result) => {
      if (result.value) {
        this.onPushOK(data);
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Cancelled',
          'Push Cancelled'
        )
      }
    })

  }

  onPushOK(data) {
    let adminData: any = JSON.parse(this.cookieService.get('adminData'))
    let body = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      push_type: "1",
      post_id: data.post_id
    }
    try {

      this._appService.postMethod('push_notification', body)
        .subscribe(resp => {
          if (resp.success) {
            swal.fire(
              'Sent!',
              'Push Notification Sent Sucessfully.',
              'success'
            )

            this.getEmployeeArticleList(0, '');
          } else {
            swal.fire(
              'Wrong!',
              'Something Went Wrong.',
              'error'
            )
            this.getEmployeeArticleList(0, '');


          }
        },
          error => {

          })
    } catch (e) { }
  }


  delarticle(data) {
    swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover the article',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        swal.fire(
          'Deleted!',
          'Your Article deleted Sucessfully.',
          'success'
        )
        this.onDeleteArticle(data);
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Cancelled',
          'Your article is safe :)',
          'error'
        )
      }
    })

  }

  getCommentsListForArticle(data) {
    let adminData: any = JSON.parse(this.cookieService.get('adminData'))
    let body = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      post_id: data.post_id,
      skip: this.approveskip,
      limit: 10,
      status: true
    }
    try {

      this._appService.postMethod('List_All_Comment_For_Article', body)
        .subscribe(resp => {
          if (resp.success) {
            this.commentData = resp.Data
            this.datasource = resp.data;
            this.length = resp.Count;


          }
        },
          error => {
          })
    } catch (e) { }
  }


  onDeleteArticle(data) {
    let adminData: any = JSON.parse(this.cookieService.get('adminData'))
    let body = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      post_id: data.post_id,
      status: 2,
    }
    try {

      this._appService.postMethod('delete_post', body)
        .subscribe(resp => {
          if (resp.success) {
            this.getEmployeeArticleList(0, '');

          } else {
          }
        },
          error => {

          })
    } catch (e) { }
  }
  onUpload() {
    this.infoMessage = null;
    this.progress = 0;
    this.isUploading = true;
  }

  fileChangeEvent(event: any, upload_type): void {
    this.imageChangedEvent = event;
    this.selectedFile = <File>event.target.files[0];

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.imgURL = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }


    this.uploading_type = upload_type
    if (event.target.value) {
      this.is_video_upload = true
    }
    if (this.uploading_type == 'morning') {
      this.morning = event
      this.a = true
    }
    if (this.uploading_type == 'afternoon') {
      this.afternoon = event
      this.b = true
    }
    if (this.uploading_type == 'evening') {
      this.evening = event
      this.c = true
    }
    if (this.uploading_type == 'night') {
      this.night = event
      this.d = true
    }
  }
  okFullImage() {
    this.uploadToServer(this.selectedFile)
    this.imageChangedEvent = ''
    this.croppedImage = ''
    this.morning_01 = ''
    this.afternoon_01 = ''
    this.evening_01 = ''
    this.night_01 = ''
    this.morning = []
    this.afternoon = []
    this.night = []
    this.evening = []
    this.imgURL = ''
  }
  okImage() {
    this.isFinalImage = true;
    this.previewImage = this.croppedImage
    if (this.uploading_type == 'morning') {
      this.morning_01 = this.croppedImage
      this.a = false
    }
    if (this.uploading_type == 'afternoon') {
      this.afternoon_01 = this.croppedImage
      this.b = false
    }
    if (this.uploading_type == 'evening') {
      this.evening_01 = this.croppedImage
      this.c = false
    }
    if (this.uploading_type == 'night') {
      this.night_01 = this.croppedImage
      this.d = false
    }
    let imag;
    let str: string = this.croppedImage
    let st4;
    if (str.indexOf('data:image/jpeg;base64,') != -1) {
      st4 = str.replace('data:image/jpeg;base64,', '')
    } else {
      st4 = str.replace('data:image/png;base64,', '')
    }
    const imageBlob = this.dataURItoBlob(st4);
    const imageFile = new File([imageBlob], 'imageName.jpeg', { type: 'image/jpeg' });
    //Once Uploaded
    this.uploadToServer(imageFile)
    this.imageChangedEvent = ''
    this.croppedImage = ''
    this.morning_01 = ''
    this.afternoon_01 = ''
    this.evening_01 = ''
    this.night_01 = ''
    this.morning = []
    this.afternoon = []
    this.night = []
    this.evening = []
  }

  okvideo() {
    this.uploadToServer(this.selectedFile)

  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // this.showCropper = true;
    // show cropper
  }

  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  onChange(event) {
    const format = 'yyyy/MM/dd';
    const formattedDate = formatDate(event, format, 'en-US');
    this.getEmployeeArticleList(0, formattedDate)
  }

  onLanguageDateChange(event) {
    const format = 'yyyy/MM/dd';
    const formattedDate = formatDate(event, format, 'en-US');

    this.getLanguageList(0, formattedDate);

  }

  getLanguages() {
    let adminData: any = JSON.parse(this.cookieService.get('adminData'))
    let body = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
    }
    try {
      this._appService.postMethod('List_All_Languages', body)
        .subscribe(resp => {
          if (resp.success) {
            this.Languages = resp.data
          } else {
          }
        },
          error => {

          })
    } catch (e) { }
  }

  getEmployeeLanguage(language_id) {
    switch (language_id) {
      case 1:
        this.language_name = 'English'
        break;
      case 2:
        this.language_name = 'हिन्दी'
        break;
      case 3:
        this.language_name = 'తెలుగు'
        break;
      case 4:
        this.language_name = 'தமிழ்'
        break;
      case 5:
        this.language_name = 'ಕನ್ನಡ'
        break;
      case 6:
        this.language_name = 'ଓଡିଆ'
        break;
      default:
        break;
    }
  }

  onRemoveImage(removeIndex) {
    this.ImageArray.splice(removeIndex, 1)
  }
  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }
  uploadToServer(imageFile) {
    const formData = new FormData();
    let url = ''
    let key = ''
    url = 'Upload_File'
    key = 'image'
    formData.append(key, imageFile);
    const req = new HttpRequest('POST', this._appService.Upload_Url + url, formData, {
      reportProgress: true,
      withCredentials: false
    });
    this._appService.onUploadFile(req)
      .subscribe(
        (event: any) => {
          if (event instanceof HttpResponse) {
            if (this.uploading_type == 1) {
              this.croppedImage = ''
              this.ImageData = []
              this.ImageData.push(event.body.extras.url)
            } else if (this.uploading_type == 2) {
              this.croppedImage = ''
              this.ImageArray.push(event.body.extras.url)
            } else if (this.uploading_type == 4) {
              this.videoData = []
              this.videoData.push(event.body.extras.url)
            } else if (this.uploading_type == 5) {
              this.videoArray.push(event.body.extras.url)
            } else if (this.uploading_type == 'morning') {
              this.GoodMorning = []
              this.GoodMorning.push(event.body.extras.url)
              this.greetingsArray.push(event.body.extras.url)
            } else if (this.uploading_type == 'afternoon') {
              this.GoodAfternoon = []
              this.GoodAfternoon.push(event.body.extras.url)
              this.greetingsArray.push(event.body.extras.url)
            } else if (this.uploading_type == 'evening') {
              this.GoodEvening = []
              this.GoodEvening.push(event.body.extras.url)
              this.greetingsArray.push(event.body.extras.url)
            } else if (this.uploading_type == 'night') {
              this.GoodNight = []
              this.GoodNight.push(event.body.extras.url)
              this.greetingsArray.push(event.body.extras.url)
            } else if (this.uploading_type == 'theme') {
              this.background_image = []
              this.background_image.push(event.body.extras.url)
            }
      
          } else if (event instanceof HttpResponse) { }
        },
        err => {
        }
      );
  }
  changeComboo(event) {
    if (event) {
      this.croppedImage = ''
      this.ImageData = []
      this.addArticleData.image = ''
      this.previewImage = ''
      this.addArticleData.title = ''
      this.addArticleData.description = ''
      this.addArticleData.is_scheduled = ''
      this.addArticleData.category_id = ''
      this.imageChangedEvent = ''
      this.imageUrl = 'https://bulma.io/images/placeholders/480x480.png'
      this.isFinalImage = false
    }
  }
  applyfilter() {
    this.getEmployeeArticleList(0, '');
  }



  getEmployeeArticleList(skip, date) {
    let body = {
      admin_id: this.adminData.id,
      session_id: this.adminData.session_id,
      skip: skip,
      limit: this.limit,
      status: 1,
      start_time: date,
      category_id: this.activateFilterButton ? this.filter.category_id : 0
    }
    try {
      this._appService.postMethod('List_Employee_Articles', body)
        .subscribe(resp => {
          if (resp.success) {
            if (this.approveskip == 0) {
            } else {
            }
            this.ApprovedArticlesList = resp.data
            const format = 'dd-MM-yyyy hh:mm:ss a';
            for (let index = 0; index < this.ApprovedArticlesList.length; index++) {
              let hello = this.ApprovedArticlesList[index];
              const formattedDate = formatDate(hello.createdAt, format, 'en-US');
              hello.createAt = formattedDate;
            }
            this.datasource = resp.data;
            this.length = resp.count;
          } else {
          }
        },
          error => {

          })
    } catch (e) { }
  }
  onNextPage(event) {
    this.currentPage_approve = event
    this.approveskip = event.pageIndex * 10
    this.getEmployeeArticleList(this.approveskip, '')
    return event
  }
  onCommentNext(event) {
    this.currentPage_approve = event
    this.approveskip = event.pageIndex * 10
    this.getCommentsListForArticle(this.approveskip)
    return event
  }
  OnNextPage(event) {
    this.currentPage_approve = event
    this.approveskip = event.pageIndex * 10
    this.getLanguageList(this.approveskip, '')
    return event
  }
  activateFilter() {
    if (this.activateFilterButton) {
      this.activateFilterButton = false
    } else {
      this.activateFilterButton = true
    }
  }
  getLanguageList(skip, date) {
    let body = {
      admin_id: this.adminData.id,
      session_id: this.adminData.session_id,
      language_id: this.adminData.language_id,
      skip: this.approveskip,
      limit: this.limit,
      status: 1,
      start_time: date,
      category_id: this.activateFilterButton ? this.filter.category_id : 0
    }
    try {
      this._appService.postMethod('List_Language_Articles', body)
        .subscribe(resp => {
          if (resp.success) {
            this.LanguageList = resp.data
            const format = 'dd-MM-yyyy hh:mm:ss a';
            for (let index = 0; index < this.LanguageList.length; index++) {
              let hello = this.LanguageList[index];
              const formattedDate = formatDate(hello.createdAt, format, 'en-US');
              hello.createAt = formattedDate;
            }
          } else {
          } this.count = resp.Count
          this.LanguageList = resp.data
          this.datasource = resp.data;
          this.Length = resp.count;
        },
          error => {
          })
    } catch (e) { }
  }
  selectedIndexChange(index: number) {
    setTimeout(() => this.selectedTabIndex = index);
    if (index === 0) {
      this.getEmployeeArticleList(0, '')
    }
    if (index === 1) {
      this.getLanguageList(0, '')
    }
  }
  open(content, data) {
    this.title_modal = data.title,
      this.popupCalls(content);
    if (data.description != "") {
      this.isDescription = true;
      this.description_modal = data.description
    } else {
      this.isDescription = false;
    }
  }
  com(content, data) {
    this.popupCalls(content);
    this.getCommentsListForArticle(data);
  }
  sticky(content, data) {
    this.commentData = data;
    this.popupCalls(content);
  }
  stickyUpdate() {
    let adminData: any = JSON.parse(this.cookieService.get('adminData'))
    let body = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      sticky_position: this.commentData.sticky_position,
      post_id: this.commentData.post_id
    }
    try {
      this._appService.postMethod('update_sticky', body)
        .subscribe(resp => {
          if (resp.success) {
            this.modalService.dismissAll
          } else {
          }
        },
          error => {

          })
    } catch (e) { }
  }
  del(content) {
    this.popupCalls(content);
  }
  popupCalls(content) {
    this.modalService.open(content).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
