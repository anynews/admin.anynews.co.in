import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublishedArticlesComponent } from './published-articles.component';


const routes: Routes = [
  {
    path: '',
    component: PublishedArticlesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublishedArticlesRoutingModule { }
