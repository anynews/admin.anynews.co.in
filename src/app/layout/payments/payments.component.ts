import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    ValidatorFn,
} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';
import { PageEvent } from '@angular/material/paginator';

@Component({
    selector: 'app-payments',
    templateUrl: './payments.component.html',
    styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {
  selected = 'Pending';
  selectedTabIndex = 0;
  activateFilterButton: Boolean = false;
  dateFilters: any = {};
  minDate = new Date();
  userData = [];
  paymentData = [];
  filter: any = {};
  payment;
  paramsData: any = {};
  adminData: any = {};
  filterData: any = {};
  districts = [];
  formattedDate: String = '';
  userarticleData: any = {};
  image = '';
  status: any = {};
  isReject = false;
  toggle = true;
//   status = 'Enable';
  currentPage_approve: any = {};
  approveskip = 0;
  pageIndex: number;
  pageSize: number;
  length: number;
  reporterlength: number;
  pageEvent: PageEvent;
  rejectReasons = [];
  display = false;
  alive = true;
  interval: any;
  data: Boolean = false;

    constructor(
        private cookieService: CookieService,
        private appService: AppService,
    ) {
        this.adminData = JSON.parse(this.cookieService.get('adminData'));
    }
    ngOnInit(): void {
        this.paramsData.admin_id = this.adminData.id;
        this.paramsData.session_id = this.adminData.session_id;
        this.getTransactions(0);
        this.filter.status = 0;
    }
    onChange(event) {
        const format = 'yyyy-MM-dd';
        this.formattedDate = formatDate(event, format, 'en-US');
        this.getTransactions(0);
    }
    getTransactions(status) {
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            status: this.filter.status ? this.filter.status : 0,
            start_time: this.formattedDate,
            skip: 0
        };
        console.log(data);
        try {
            this.appService.postMethod('get_user_transactions', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.userData = resp.data;
                        const format = 'dd-MM-yyyy hh:mm:ss a';
                    for (let index = 0; index < this.userData.length; index++) {
                      const hello = this.userData[index];
                      const formattedDate = formatDate(hello.createdAt, format, 'en-US');
                      hello.createAt = formattedDate;
                    }
                    if (this.filter.status === 2 || this.filter.status === 3) {
                      for (let index = 0; index < this.userData.length; index++) {
                        const hello = this.userData[index];
                        const formattedDate = formatDate(hello.transaction_time, format, 'en-US');
                        hello.transactiontime = formattedDate;
                      }
                    }
                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => { }
            );
        } catch (e) { }
    }

    onNextPage(event) {
        console.log(event);
        this.currentPage_approve = event;
        this.approveskip = event.pageIndex * 10;
        console.log(this.approveskip, 'Skip');
        this.getTransactions(this.filter.Status);
        return event;
      }
    approve(data) {
        swal.fire({
          title: 'Are you sure?',
          text: 'You want to Approve the Payment',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Approve',
          cancelButtonText: 'Reject'
        }).then((result) => {
          if (result.value) {
            swal.fire(
              'Approved!',
              'Your Payment Approved Sucessfully.',
              'success'
            );
            this.approve_payment(data.id);

          } else if (result.dismiss === swal.DismissReason.cancel) {
            swal.fire(
              'Cancelled',
              'error'
            );
          }
        });

      }

      cancel(data) {
        swal.fire({
          title: 'Are you sure?',
          text: 'You want to Cancel the Payment',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Ok',
          cancelButtonText: 'Cancel'
        }).then((result) => {
          if (result.value) {
            swal.fire(
              'Your Payment Cancelled Sucessfully.',
              'success'
            );
            this.cancel_payment(data.id);
          } else if (result.dismiss === swal.DismissReason.cancel) {
            swal.fire(
              'Cancelled',
              'error'
            );
          }
        });

      }

    approve_payment(id) {
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            status: 1,
            payment_id: id
        };
        console.log(data);
        try {
            this.appService.postMethod('approve_payment', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.userData = resp.data;
                        console.log(resp);
                        this.userData = resp.data;
                        this.getTransactions(this.filter.status);
                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => { }
            );
        } catch (e) { }
    }

    cancel_payment(id) {
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            payment_id: id
        };
        console.log(data);
        try {
            this.appService.postMethod('cancel_payment', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.userData = resp.data;
                        console.log(resp);
                        this.userData = resp.data;

                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => { }
            );
        } catch (e) { }
    }

}
