import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { formatDate } from '@angular/common';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
    paramsData: any = {};
    adminData: any;
    dateFilters: any = {};
    minDate = new Date();
    pushNotificationsCount: any;
    accuracyCount: any;
    articlesCount: any;
    firstPostTime: any;
    filter: any = {}
    languageData = [];

    public barChartOptions: ChartOptions = {
        responsive: true,
        tooltips: {
            mode: 'x',
            intersect: false,
        },
        scales: { xAxes: [{}], yAxes: [{}] },
        plugins: { datalabels: { anchor: 'end', align: 'end' } },
    };
    
    public barChartPlugins = [pluginDataLabels];
    public barChartLegend = true;
    public barChartType: ChartType = 'bar';
    public barChartLabels: Label[] = [];
    public barChartData: any = [
        {
            data: [],
            label: 'Push Notifications Count',
            backgroundColor: ['#de63ff','#003f5c','#444e86','#ffa600','#dd5182','#ff6e54'],
        },
    ];


    public barChartOptions1: ChartOptions = {
        responsive: true,
        tooltips: {
            mode: 'x',
            intersect: false,
        },

        scales: { xAxes: [{}], yAxes: [{}] },
        plugins: { datalabels: { anchor: 'end', align: 'end' } },
    };
    // public barChartPlugins = [pluginDataLabels];
    public barChartLegend1 = true;
    public barChartType1: ChartType = 'bar';
    public barChartLabels1: Label[] = [];
    public barChartData1: any = [
        { data: [], label: 'Accuracy Count', backgroundColor: ['#de63ff','#003f5c','#444e86','#ffa600','#dd5182','#ff6e54'], },
    ];
    public barChartOptions2: ChartOptions = {
        responsive: true,
        tooltips: {
            mode: 'x',
            intersect: false,
        },

        scales: { xAxes: [{}], yAxes: [{}] },
        plugins: { datalabels: { anchor: 'end', align: 'end' } },
    };
    // public barChartPlugins = [pluginDataLabels];
    public barChartLegend2 = true;
    public barChartType2: ChartType = 'bar';
    public barChartLabels2: Label[] = [];
    public barChartData2: any = [
        { data: [], label: 'Articles Count', backgroundColor: ['#de63ff','#003f5c','#444e86','#ffa600','#dd5182','#ff6e54'], },
    ];

    constructor(
        private cookieService: CookieService,
        private appService: AppService,
        public router: Router
    ) {
        if (this.cookieService.get('adminData')) {
            this.adminData = JSON.parse(this.cookieService.get('adminData'));
        } else {
            this.adminData = {};
            this.router.navigate(['/login']);
        }
    }
    ngOnInit() {
        this.dateFilters.date = new Date();
        this.paramsData.admin_id = this.adminData.id;
        this.paramsData.session_id = this.adminData.session_id;

        const format = 'yyyy/MM/dd';
        const formattedDate = formatDate(this.minDate, format, 'en-US');
        this.getPushNotificationsCount();
        this.getAccuracyCount();
        this.getarticlesCount();
    }
    languages(data) {
        try {
            this.appService.postMethod('List_All_Languages', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.languageData = resp.data;
                        console.log(this.languageData,);
                    } else {
                        swal.fire(resp.msg, 'error');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }

    getPushNotificationsCount() {
        try {
            const paramsObj = {};
            paramsObj['admin_id'] = this.adminData.id;
            paramsObj['session_id'] = this.adminData.session_id;
            // console.log(paramsObj);
            this.pushNotificationsCount = [];
            this.barChartLabels= [];
            this.barChartData[0].data=[];
            this.appService
                .postMethod('get_push_notifications_count', paramsObj)
                .subscribe(
                    (resp: any) => {
                        if (resp.success) {
                            this.pushNotificationsCount = resp.data;
                            this.pushNotificationsCount.forEach((element) => {
                                this.barChartLabels.push(element.language);
                                this.barChartData[0].data.push(element.count);
                            });
                        } else {
                            swal.fire(resp.msg, '');
                        }
                    },
                    (error) => {}
                );
        } catch (e) {}
    }
    getAccuracyCount() {
        try {
          
            const paramsObj = {};
            paramsObj['admin_id'] = this.adminData.id;
            paramsObj['session_id'] = this.adminData.session_id;
            // console.log(paramsObj);
            this.accuracyCount = [];
            this.barChartLabels1= [];
            this.barChartData1[0].data=[];
            this.appService
                .postMethod('get_accuracy_count', paramsObj)
                .subscribe(
                    (resp: any) => {
                        if (resp.success) {
                            this.accuracyCount = resp.data;
                            // console.log(this.accuracyCount);

                            this.accuracyCount.forEach((element) => {
                                this.barChartLabels1.push(element.language);
                                this.barChartData1[0].data.push(element.count);
                            });
                        } else {
                            swal.fire(resp.msg, '');
                        }
                    },
                    (error) => {}
                );
        } catch (e) {}
    }
    getarticlesCount() {
        try {
            const paramsObj = {};
            paramsObj['admin_id'] = this.adminData.id;
            paramsObj['session_id'] = this.adminData.session_id;
            // console.log(paramsObj);
            this.articlesCount = [];
            this.barChartLabels2= [];
            this.barChartData2[0].data=[];
            this.appService
                .postMethod('get_articles_count', paramsObj)
                .subscribe(
                    (resp: any) => {
                        if (resp.success) {
                            this.articlesCount = resp.data;
                            this.articlesCount.forEach((element) => {
                                this.barChartLabels2.push(element.language);
                                this.barChartData2[0].data.push(element.count);
                            });
                        } else {
                            swal.fire(resp.msg, '');
                        }
                    },
                    (error) => {}
                );
        } catch (e) {}
    }
    getFirstPostTime() {
        try {
            this.firstPostTime = [];
            const paramsObj = {};
            paramsObj['admin_id'] = this.adminData.id;
            paramsObj['session_id'] = this.adminData.session_id;
            // console.log(paramsObj);
            this.appService
                .postMethod('get_first_post_time', paramsObj)
                .subscribe(
                    (resp: any) => {
                        if (resp.success) {
                            this.firstPostTime = resp.data;
                            // this.firstPostTime.forEach((element) => {
                            //     this.barChartLabels2.push(element.language);
                            //     this.barChartData2[0].data.push(element.count);
                            // });
                        } else {
                            swal.fire(resp.msg, '');
                        }
                    },
                    (error) => {}
                );
        } catch (e) {}
    }
    onChange(event) {
        const format = 'yyyy/MM/dd';
        const formattedDate = formatDate(event, format, 'en-US');
        this.getPushNotificationsCount();
        this.getAccuracyCount();
        this.getarticlesCount();
        this.getFirstPostTime();
    }
    language(value) {
        console.log(value);
        // this.languages();
        console.log("District Status")
      }
}
