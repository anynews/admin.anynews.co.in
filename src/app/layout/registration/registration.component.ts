import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { Body } from '@angular/http/src/body';
import { PageEvent } from '@angular/material/paginator';


import {
  NgbModal,
  NgbModalConfig,
  ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class RegistrationComponent implements OnInit {
  hide = true;
  registrationData: any = {};
  permissionsData: any = {};
  adminData: any;
  paramsData: any = {};
  RegistrationForm: FormGroup;
  isShown = false;
  showMe = true;
  optionValue = '';
  optionalValue = '';
  pageEvent: PageEvent;
  approveskip = 0;
  limit = 10;
  skip = 0;
  datasource: null;
  pageIndex: number;
  pageSize: number;
  length: number;
  isEdit: any = {};
  currentPage_approve: any = {};
  Telengana = '';
  is_admin = '';
  role = '';
  isAdminListLoadin: boolean;
  LanguageList: any;
  title = 'appBootstrap';
  model;
  closeResult: string;
  languageData = []
  employeeData: any;
  employee_new_id: any = {};
  addLanguageData: any = {};
  LoginForm: FormGroup;
  DeleteForm: FormGroup;
  delete_employee_data: any = {};
  delete_employee: any = {};
  selectedTabIndex = 0;
  constructor(private router: Router,
    private appService: AppService,
    private cookieService: CookieService,
    private _appService: AppService,
    private modalService: NgbModal,
  ) {
    this.adminData = JSON.parse(this.cookieService.get('adminData'));
  }
  // {
  //   this.getLanguageList()
  // };
  roles = {

  };



  ngOnInit() {
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
    this.List_All_Languages(this.paramsData);
    this.List_All_Employees(0);
    // this.getLanguageList(0);

  }

  selectedIndexChange(index: number) {
    setTimeout(() => this.selectedTabIndex = index);
    if (index === 1) {
      this.List_All_Employees(0);
    }
  }
  List_All_Languages(data) {
    try {
        this.appService.postMethod('List_All_Languages', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.languageData = resp.data;
                    console.log(resp.data,"Deepik");
                } else {
                    swal.fire(resp.msg, 'error');
                }
            },
            (error) => { }
        );
    } catch (e) { }
}
  List_All_Employees(skip) {
    console.log(this.employeeData);
    const data = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      skip: skip,
      limit: 10,
    };
    try {
        this.appService.postMethod('List_All_Employees', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.employeeData = resp.data;
                    this.length = resp.count;

                    console.log(resp);
                } else {
                    swal.fire(resp.msg, 'error');
                }
            },
            (error) => { }
        );
    } catch (e) { }
}
onNextPage(event) {
  console.log(event);
  this.currentPage_approve = event;
  this.approveskip = event.pageIndex * 10;
  console.log(this.approveskip, 'Skip');
  this.List_All_Employees(this.approveskip);
  return event;
}
// editForm() {
//   const data = {
//       admin_id: this.paramsData.admin_id,
//       session_id: this.paramsData.session_id,
//       language_id: this.employee_new_id.language_id,
//       district_id: this.employee_new_id.id,
//       name: this.employee_new_id.name,

//       // category_wise_titles: this.categoryData.category_wise_title,
//   };
//   console.log(data);
//   try {
//       this.appService.postMethod('edit_employee', data).subscribe(
//           (resp: any) => {
//               if (resp.success) {
//                   this.employeeData = resp.msg;
//                   console.log(resp);
//               } else {
//                   swal.fire(resp.msg, '');
//               }
//           },
//           (error) => {}
//       );
//   } catch (e) {}
// }

  onEditClick(event) {
    console.log(event, '<<<<');
    this.optionValue = event;
  }
  onEditClickk(event) {
    console.log(event, '<<<<');
    this.optionalValue = event;
  }
  changeClient(event) {
    console.log(event);
    console.log('Hello');
  }

  toggleTag() {
    if (this.showMe === true) {
      this.showMe = false;
    } else {
      this.showMe = true;
    }
    console.log('>>>', this.showMe);
    //  this.showMe = !this.showMe;
    console.log('>>>', this.showMe);

  }
  create() {
    console.log('Created!!!', this.registrationData);
    // JSON.parsethis.cookieService.get('adminData');
    const adminData = JSON.parse(this.cookieService.get('adminData'));
    const body = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      name: this.registrationData.name,
      email_id: this.registrationData.email_id,
      pwd: this.registrationData.password,
      is_admin: this.registrationData.is_admin == "true"? true : false,
      permissions: this.permissionsData,
      language_id: this.registrationData.language_id,
      role: this.registrationData.role,

    };
    try {
      this.appService.postMethod('Create_Admin_User', body)
        .subscribe((resp: any) => {
          if (resp.success) {
            this.registrationData = {};
            this.permissionsData = {};
            swal.fire('Employee Created Succesfully');

          } else {
            swal.fire('Enter All Tags', 'Something went wrong!', 'error');
          }
        },
          error => {
          });
    } catch (e) { }
  }

delForm() {
  console.log('Employee Deleted!!!');
  const data = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      employee_id: this.delete_employee_data.id,
      status: false,

  };
  console.log(data);
  try {
      this.appService.postMethod('Active_Inactive_Employee', data).subscribe(
          (resp: any) => {
              if (resp.success) {
                  this. delete_employee = resp.msg;
                  console.log('Deleted');
              } else {
                  swal.fire(resp.msg, '');
              }
          },
          (error) => {}
      );
  } catch (e) {}
}




OnNextPage(event) {
  console.log(event);
  this.currentPage_approve = event;
  this.approveskip = event.pageIndex * 10;
  console.log(this.approveskip, 'Skip');
  // this.getLanguageList(this.approveskip);
  return event;
}
EditEmployee(event, data) {
  this.router.navigate(['edit-employee'], data);
}



open(content) {
  this.popupCalls(content);

}

del(content, data) {
  this.delete_employee_data = data,
  console.log(data),
  console.log(this.delete_employee_data);

  this.popupCalls(content);
}

popupCalls(content) {
  console.log('hllo', content);
  this.modalService.open(content).result.then(
      (result) => {
          this.closeResult = `Closed with: ${result}`;
          console.log('in close', content);
      },
      (reason) => {
          console.log('in dismis');
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
  );
}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
  } else {
      return `with: ${reason}`;
  }
}


}
