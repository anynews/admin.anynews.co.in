import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PullBackComponent } from './pull-back.component';

describe('PullBackComponent', () => {
  let component: PullBackComponent;
  let fixture: ComponentFixture<PullBackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PullBackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PullBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
