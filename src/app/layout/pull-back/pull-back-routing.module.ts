import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PullBackComponent } from './pull-back.component';


const routes: Routes = [
  {
    path : '',
    component : PullBackComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PullBackRoutingModule { }
