import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TestingRoutingModule } from './testing-routing.module';
import { TestingComponent } from './testing.component';


@NgModule({
  declarations: [TestingComponent],
  imports: [
    CommonModule,
    TestingRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    NgbModule
  ]
})
export class TestingModule { }
