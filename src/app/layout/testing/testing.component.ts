import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from "@ng-bootstrap/ng-bootstrap";
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    ValidatorFn,
} from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { AppService } from "src/app/app.service";
import swal from "sweetalert2";
import { MatDialog, MatDialogConfig, } from '@angular/material/dialog';
import { HttpRequest, HttpResponse } from '@angular/common/http';

@Component({
    selector: "app-testing",
    templateUrl: "./testing.component.html",
    styleUrls: ["./testing.component.scss"],
    providers: [NgbModalConfig, NgbModal],
})
export class TestingComponent implements OnInit {
    profileData: any = {};
    paramsData: any = {};
    adminData: any;
    title = "appBootstrap";
    model;
    closeResult: string;
    editData: any = {};
    profile_new_id: any = {};
    ProfileForm: FormGroup;
    PWDForm: FormGroup;
    text: string;
    name = 'Angular 4';
    url: any = {}
    selectedFile: File = null;
    image:Boolean = false;
    constructor(
        private cookieService: CookieService,
        private appService: AppService,
        private http: HttpClient,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private dialog: MatDialog,
        private _appService: AppService,



    ) {
        this.adminData = JSON.parse(this.cookieService.get("adminData"));
    }

    ngOnInit() {
        this.paramsData.admin_id = this.adminData.id;
        this.paramsData.session_id = this.adminData.session_id;
        this.view_profile();
    }
    onCreate() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        this.dialog.open(TestingComponent, dialogConfig);
    }
    view_profile() {
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            user_id: this.paramsData.admin_id,
        };
        console.log(data);
        try {
            this.appService.postMethod("view_profile", data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.profileData = resp.data;
                        console.log(this.profileData);
                    } else {
                        swal.fire(resp.msg, "error");
                    }
                },
                (error) => { }
            );
        } catch (e) { }
    }


    editProfileForm() {
        console.log("Edit Profile");
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            user_id: this.paramsData.admin_id,
            profile_pic: this.url,
            name: this.profile_new_id.name,
            email_id: this.profileData[0].email_id
        };
        console.log(data);
        try {
            this.appService.postMethod('edit_profile', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.modalService.dismissAll();
                        this.profileData = resp.msg;
                        console.log(resp);
                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => { }
            );
        } catch (e) { }
    }
    editPWDForm() {
        console.log("Edit Password");
        // const data = {
        //     admin_id: this.paramsData.admin_id,
        //     session_id: this.paramsData.session_id,
        //     title: this.district_new_id.title,
        //     state_id: this.district_new_id.state_id,
        //     dist_number: this.district_new_id.dist_number,
        //     district_id: this.district_new_id.id,
        //     name: this.district_new_id.name,
        // };
        // console.log(data);
        // try {
        //     this.appService.postMethod('edit_profile', data).subscribe(
        //         (resp: any) => {
        //             if (resp.success) {
        //                 this.profileData = resp.msg;
        //                 console.log(resp);
        //             } else {
        //                 swal.fire(resp.msg, '');
        //             }
        //         },
        //         (error) => {}
        //     );
        // } catch (e) {}
    }
    onSelectFile(event) {
        this.selectedFile = <File>event.target.files[0];

        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();

            reader.readAsDataURL(event.target.files[0]); // read file as data url

            reader.onload = (event) => { // called once readAsDataURL is completed
                this.url = event.target.result;
                this.image = true;
            }
        }
    }
    uploadProfilepic() {
        console.log("deepik")
        this.uploadToServer(this.selectedFile);
    }
    open(content) {
        // this.district_new_id = data,
        // console.log(this.district_new_id);
        this.popupCalls(content);   
    }
    del(content, data) {
        // this.delete_districts = data,
        // console.log(this.delete_districts);
        this.popupCalls(content);
    }
    popupCalls(content) {
        console.log("hllo", content);
        this.modalService.open(content).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
                console.log("in close", content);
            },
            (reason) => {
                console.log("in dismis");
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }




    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return "by pressing ESC";
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return "by clicking on a backdrop";
        } else {
            return `with: ${reason}`;
        }
    }



    uploadToServer(imageFile) {
        const formData = new FormData();
        let url = ''
        let key = ''
        url = 'Upload_File'
        key = 'image'
        formData.append(key, imageFile);
        const req = new HttpRequest('POST', this._appService.Upload_Url + url, formData, {
            reportProgress: true,
            withCredentials: false
        });
        this._appService.onUploadFile(req)
            .subscribe(
                (event: any) => {
                    if (event instanceof HttpResponse) {
                        this.url = (event.body.extras.url)

                        swal.fire("Image Uploaded Succesfully");
                        this.image = false;
                        // else if (type == 3) {
                        //   this.isUploadingArrayBtnLoading = false
                        //   this.nzMessageService.success('Video Upoladed Sucessfully')
                        //   this.croppedImage = ''
                        //   this.imgURL = ''
                        //   this.video = ''
                        //   this.myInputVariable.nativeElement.value = "";
                        //   this.video = event.body.extras.url
                        // }
                        // else if (this.UploadingType == 4) {
                        //   this.isUploadTheme = false
                        //   this.nzMessageService.success('Image Theme Upload Sucessfully')
                        //   this.croppedImage = ''
                        //   this.imgURL = ''
                        //   this.myInputVariable.nativeElement.value = "";
                        //   this.themeURL=event.body.extras.url
                        // }
                    } else if (event instanceof HttpResponse) { }
                },
                err => {
                }
            );
    }
}
