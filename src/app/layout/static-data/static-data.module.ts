import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatTabsModule} from '@angular/material/tabs';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StaticDataRoutingModule } from './static-data-routing.module';
import { StaticDataComponent } from './static-data.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [StaticDataComponent],
  imports: [
    CommonModule,
    StaticDataRoutingModule,
    MatTabsModule,
    MatCardModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    TranslateModule,

  ]
})
export class StaticDataModule { }

