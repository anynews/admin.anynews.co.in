import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    ValidatorFn,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';
import { lang } from 'moment';

@Component({
  selector: 'app-static-data',
  templateUrl: './static-data.component.html',
  styleUrls: ['./static-data.component.scss'],
    providers: [NgbModalConfig, NgbModal],
})
export class StaticDataComponent implements OnInit {
    // @ViewChild(MatTabGroup) tabGroup: MatTabGroup;

    isAdminListLoadin = false;
    LanguageList = [];
    LanguageID: any;
    skip = 0;
    limit = 10;
    languages_number: any = 0;

    title = 'appBootstrap';
    model;
    closeResult: string;
    paramsData: any = {};
    adminData: any;
    languageData = [];
    staticData: any = {};
    LoginForm: FormGroup;
    EditStaticDataForm: FormGroup;
    CreateStaticData:FormGroup;
    create_static_data: any = {}
    selectedTabIndex = 0;
    static_data_count = 0;
    static_data = []
    edit_static_data :any = {}
    language_id = 1
    constructor(
        private cookieService: CookieService,
        private appService: AppService,
        private location: Location,
        // private route: ActivatedRoute,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private http: HttpClient
    ) {
        this.adminData = JSON.parse(this.cookieService.get('adminData'));
    }

    ngOnInit() {
        this.paramsData.admin_id = this.adminData.id;
        this.paramsData.session_id = this.adminData.session_id;
        this.List_All_Languages(this.paramsData);
        this.view_static_data(1)
    }

    selectedIndexChange(index: number) {
        setTimeout(() => this.selectedTabIndex = index);
        if (index === 0) {
            this.language_id = 1
            this.view_static_data(this.language_id)
      
          }
        if (index === 1) {
            this.language_id = 2
            this.view_static_data(this.language_id)    
        }
        if (index === 2) {
          console.log('Telugu');
          this.language_id = 3
          this.view_static_data(this.language_id)

          // this.Employee_List(2);
        }
        if (index === 3) {
          console.log('Tamil');
          this.language_id = 4
          this.view_static_data(this.language_id)
        }
        if (index === 4) {
            console.log('Kannada');
            this.language_id = 5
            this.view_static_data(this.language_id)
          }
      }
    

      view_static_data(Language_ID) {
          let data = {
              admin_id : this.paramsData.admin_id,
              session_id : this.paramsData.session_id,
              language_id: this.language_id
          }
        try {
            this.static_data = []

            this.appService.postMethod('Admin_Fetch_Static_Data', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.static_data_count = resp.count;
                        this.static_data=resp.data;
                        console.log(this.static_data);

                    } else {
                        this.static_data = []
                        swal.fire(resp.msg, 'Oops!! No Data');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }
    




    





    










    




    //Edit Static Data
    edit_staticData(){
        let data = {
            admin_id : this.paramsData.admin_id,
            session_id : this.paramsData.session_id,
            static_id : this.edit_static_data.static_id,
            language_id : this.language_id,
            title : this.edit_static_data.title,
            name : this.edit_static_data.name
        }
      try {
          this.appService.postMethod('Edit_Static_Language', data).subscribe(
              (resp: any) => {
                  if (resp.sucess) {
                    this.ngOnInit()
                      swal.fire(resp.msg, 'Success');
                  } else {
                      swal.fire(resp.msg, 'Error');
                  }
              },
              (error) => {}
          );
      } catch (e) {}



        
    }


    //Create Static Data
    create_staticData(){
        console.log(this.create_static_data,"Create Data")
        let data = {
            admin_id : this.paramsData.admin_id,
            session_id : this.paramsData.session_id,
            static_id : this.edit_static_data.static_id,
            language_id : this.edit_static_data.language_id,
            title : this.create_static_data.title,
            name : this.create_static_data.name
        }
      try {
          this.appService.postMethod('Add_Static_Language', data).subscribe(
              (resp: any) => {
                  if (resp.sucess) {
                      swal.fire(resp.msg, 'Success');
                  } else {
                      swal.fire(resp.msg, 'Error');
                  }
              },
              (error) => {}
          );
      } catch (e) {}



        
    }


    List_All_Languages(data) {
        try {
            this.appService.postMethod('List_All_Languages', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.languageData = resp.data;
                        console.log(this.languageData,);
                    } else {
                        swal.fire(resp.msg, 'error');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }

    submitForm() {
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            language_id: this.staticData.language_id,
            static_id: this.staticData.static_id,
            name: this.staticData.static_name,
            title: this.staticData.title,

        };
        console.log(data);
        try {
            this.appService.postMethod('Edit_Category', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.staticData = resp.msg;
                        console.log(resp);
                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }

    create(content){
        this.popupCalls(content);

    }
    open(content,data) {
        this.edit_static_data = data
        console.log(this.edit_static_data)

      this.popupCalls(content);
  }
  popupCalls(content) {
      console.log('hllo', content);
      this.modalService.open(content).result.then(
          (result) => {
              this.closeResult = `Closed with: ${result}`;
            //   console.log('in close', content);
          },
          (reason) => {
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          }
      );
  }





  private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
      } else {
          return `with: ${reason}`;
      }
  }

}

