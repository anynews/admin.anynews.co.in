import { Location } from '@angular/common';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    ValidatorFn,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';

@Component({
    selector: 'app-locations',
    templateUrl: './locations.component.html',
    styleUrls: ['./locations.component.scss'],
    providers: [NgbModalConfig, NgbModal],
})
export class LocationsComponent implements OnInit {
    title = 'appBootstrap';
    model;
    closeResult: string;
    paramsData: any = {};
    adminData: any;
    languageData: any = {};
    locationData: any = {};
    locationDataNew: any;
    editData: any = {};
    LoginForm: FormGroup;
    deleteDistrictsForm: FormGroup;
    stateData: any = {};
    stateDataNew: any;
    state_new_id = '';
    district_new_id: any = {};
    delete_districts: any = {};
    view: boolean;
    states: any;
    clickMessage: Boolean = false;
    Newhello(title) {
        this.clickMessage = true;
        // this.districts(state)
        console.log(this.clickMessage);
    }

    constructor(
        private cookieService: CookieService,
        private appService: AppService,
        private location: Location,
        // private route: ActivatedRoute,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private http: HttpClient
    ) {
        this.adminData = JSON.parse(this.cookieService.get('adminData'));
    }

    ngOnInit() {
        this.paramsData.admin_id = this.adminData.id;
        this.paramsData.session_id = this.adminData.session_id;
        this.locationData.state_id = this.locationData.state_id;
        this.view_states();
    }
    view_states() {
        try {
            this.appService.getMethod('view_states').subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.stateDataNew = resp.data;
                        console.log(resp);
                    } else {
                        swal.fire(resp.msg, 'error');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }
    List_All_Languages(data) {
        try {
            this.appService.postMethod('List_All_Languages', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.languageData = resp.data;
                        console.log(resp);
                    } else {
                        swal.fire(resp.msg, 'error');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }
    view_districts(state_id) {
        this.state_new_id = state_id;
        console.log(state_id, 'State Id');
        this.clickMessage = true;
        console.log(this.clickMessage);
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            state_id: state_id,
        };
        console.log(data);
        try {
            this.appService.postMethod('view_districts', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.locationDataNew = resp.data;
                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }


    addForm(state_id) {
        console.log(this.locationData, 'Create District');
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            name: this.locationData.name,
            state_id: this.state_new_id,
            dist_number: this.locationData.dist_number,
            title: this.locationData.title,
        };
      try {
          this.appService.postMethod('add_district', data).subscribe(
              (resp: any) => {
                  if (resp.success) {
                      swal.fire(resp.msg, 'Success');
                      this.modalService.dismissAll()
                  } else {
                      swal.fire(resp.msg, 'Error');
                  }
              },
              (error) => {}
          );
      } catch (e) {}

    }



    editDistrictForm() {
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            title: this.district_new_id.title,
            state_id: this.district_new_id.state_id,
            dist_number: this.district_new_id.dist_number,
            district_id: this.district_new_id.id,
            name: this.district_new_id.name,

            // category_wise_titles: this.categoryData.category_wise_title,
        };
        console.log(data);
        try {
            this.appService.postMethod('edit_district', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        swal.fire(resp.msg, 'Success');
                        this.modalService.dismissAll();

                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }

    delDistricts() {
        console.log(this.delete_districts);
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,

            state_id: this.delete_districts.state_id,
            status: false,
            district_id: this.delete_districts.id,


        };
        console.log(data);
        try {
            this.appService.postMethod('Active_Inactive_District', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.delete_districts = resp.msg;
                        swal.fire(resp.msg, 'Deleted');
                        this.modalService.dismissAll();   
                        console.log('Deleted');
                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }



    edit_state(data) {
        try {
            this.appService.postMethod('edit_district', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.locationData = resp.msg;
                    } else {
                        swal.fire(resp.msg, 'error');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }
    editStateForm() {
        const data = {
            admin_id: this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            name: this.locationData.name,
            title: this.locationData.title,
            language_id: this.locationData.language_id,

            // dist_number: this.locationData.dist_number,
            // dist_id: this.locationData.dist_id
            // category_wise_titles: this.categoryData.category_wise_title,
        };
        console.log(data);

        try {
            this.appService.postMethod('edit_state', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.locationData = resp.msg;
                        console.log(resp);
                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }
  create(content) {
        this.popupCalls(content);

    }
    open(content, data) {
        this.district_new_id = data,
        console.log(this.district_new_id);
        this.popupCalls(content);
    }
    del(content, data) {
        this.delete_districts = data,
        console.log(this.delete_districts);
        this.popupCalls(content);
    }
    popupCalls(content) {
        console.log('hllo', content);
        this.modalService.open(content).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
                console.log('in close', content);
            },
            (reason) => {
                console.log('in dismis');
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
}
