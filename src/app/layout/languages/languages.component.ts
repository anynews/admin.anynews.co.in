import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    ValidatorFn,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';


@Component({
    selector: 'app-languages',
    templateUrl: './languages.component.html',
    styleUrls: ['./languages.component.scss'],
    providers: [NgbModalConfig, NgbModal],
})
export class LanguagesComponent implements OnInit {
    title = 'appBootstrap';
    model;
    closeResult: string;
    paramsData: any = {};
    adminData: any;
    languageData: any;
    addLanguageData: any = {};
    LoginForm: FormGroup;

    constructor(
        private cookieService: CookieService,
        private appService: AppService,
        private location: Location,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private http: HttpClient,
        public router: Router
    ) {
        this.adminData = JSON.parse(this.cookieService.get('adminData'));
    }

    ngOnInit() {
        this.paramsData.admin_id = this.adminData.id;
        this.paramsData.session_id = this.adminData.session_id;
        this.List_All_Languages(this.paramsData);


    }
    List_All_Languages(data) {
        try {
            this.appService.postMethod('List_All_Languages', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        console.log(resp);
                        this.languageData = resp.data;
                    } else {
                        swal.fire(resp.msg, 'error');
                    }
                },
                (error) => { }
            );
        } catch (e) { }
    }
    submitForm() {
        const data = {
            admin_id : this.paramsData.admin_id,
            session_id: this.paramsData.session_id,

            title: this.addLanguageData.title,
            name: this.addLanguageData.name,
            type: this.addLanguageData.status,
        };
        console.log(data);
        try {
            this.appService.postMethod('Add_Language', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.addLanguageData = resp.msg;
                        console.log(resp);
                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }

    editForm() {
        const data = {
            admin_id : this.paramsData.admin_id,
            session_id: this.paramsData.session_id,
            language_id: this.addLanguageData.language_id,
            title: this.addLanguageData.title,
            name: this.addLanguageData.name,
            language_type: this.addLanguageData.status,
        };
        console.log(data);
        try {
            this.appService.postMethod('Update_Language', data).subscribe(
                (resp: any) => {
                    if (resp.success) {
                        this.addLanguageData = resp.msg;
                        console.log(resp);
                    } else {
                        swal.fire(resp.msg, '');
                    }
                },
                (error) => {}
            );
        } catch (e) {}
    }


    open(content) {
        this.popupCalls(content);
    }
    popupCalls(content) {
        console.log('hllo', content);
        this.modalService.open(content).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
                console.log('in close', content);
            },
            (reason) => {
                console.log('in dismis');
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }


}
