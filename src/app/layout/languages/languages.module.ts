import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { LanguagesRoutingModule } from './languages-routing.module';
import { LanguagesComponent } from './languages.component';
import {MatSelectModule} from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';




@NgModule({
  declarations: [LanguagesComponent],
  imports: [
    CommonModule,
    MatInputModule,
    LanguagesRoutingModule,
    MatSelectModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    TranslateModule,
  ]
})
export class LanguagesModule { }
